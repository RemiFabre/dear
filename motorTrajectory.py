import utils
import time
import copy

class MotorTrajectory():
    def __init__(self, motor, listOfTrajs, listOfTorqueTrajs, listOfDurations, mode=3, repeat=False, delay=0.1) :
#         if (listOfDurations[-1] == 0) :
#             #To Do : fix this known bug. Hot fix below. Ok, this is now fixed, keeping this to maintain old serialized files working.
#             listOfDurations = listOfDurations[:-1]
#             listOfTrajs = listOfTrajs[:-1]
        if (len(listOfTorqueTrajs) != 0 and len(listOfTorqueTrajs) != len(listOfTrajs)) :
            print "listOfTrajs = ", listOfTrajs, "listOfTorqueTrajs = ", listOfTorqueTrajs, "listOfDurations = ", listOfDurations 
            raise ValueError('In MotorTrajectory, ListOfTorqueTrajs ({0}) is not empty and has a different size than listOfTrajs ({1}).'.format(len(listOfTorqueTrajs), len(listOfTrajs)))
        if (len(listOfDurations) != len(listOfTrajs)) :
            raise ValueError('In MotorTrajectory, ListOfDurations has not the same size than listOfTrajs.')
#         print "Creating traj : motor id = {}\nlistOftrajs = {}\nlistOfTorqueTrajs = {}\nlistOfDurations = {}\nmode = {}".format(motor.id, listOfTrajs, listOfTorqueTrajs, listOfDurations, mode)
        self.motor = motor
        self.listOfTrajs = copy.deepcopy(listOfTrajs)
        self.listOfTorqueTrajs = copy.deepcopy(listOfTorqueTrajs)
        self.listOfDurations = copy.deepcopy(listOfDurations)
        self.mode = mode
        self.repeat = repeat
        self.delay = delay
        
        self.trajId = 0
        self.over = False
        self.stopped = False
        self.t0 = 0
        
    def start(self) :
        self.over = False
        self.stopped = False
        self.trajId = 0
        # Taking care of the offsets of each motor 
        for i in range(0, len(self.listOfTrajs)):
            self.listOfTrajs[i][0] = self.listOfTrajs[i][0] + ((self.motor.offset + 180)%360)*4096/360.0
            
        #Setting goal position to the final value of the traj so it'll end smoothly (formula to be checked)
#         finalValue = evalPoly(self.listOfDurations[0]/10000.0, self.listOfTrajs[0]) - ((self.motor.offset + 180)%360)*4096/360.0
#         self.motor.goal_position = finalValue*360.0/4096.0 - ((self.motor.offset + 180)%360)

        utils.setTraj1ToMotor(self.motor, self.listOfTrajs[0], self.listOfDurations[0], False, self.mode)
        if (len(self.listOfTorqueTrajs) > 0):
            utils.setTorqueTraj1ToMotor(self.motor, self.listOfTorqueTrajs[0])
            
        if (len(self.listOfTrajs) > 1) :
            self.trajId = 1
        else :
            self.over = True 
        self.t0 = time.time()
        
    def tick(self) :
        if (self.over != True) :
            currentTime = time.time() - self.t0
            if (self.motor.copy_next_buffer_read == 0 and currentTime > self.delay) :
                #Need to wait a bit before checking the flag copy_next_buffer_read because this loop is faster than the low-level sync loop
                #so the flag value might not be up to date when reading it
                self.t0 = time.time()
                # The previous traj2 has been copied into traj1, its time to update traj2
                utils.continueTrajWithMotor(self.motor, 
                                  self.listOfTrajs[self.trajId], 
                                  self.listOfDurations[self.trajId], self.mode)
                
                #Setting goal position to the final value of the traj so it'll end smoothly (formula to be checked)
#                 finalValue = evalPoly(self.listOfDurations[self.trajId]/10000.0, self.listOfTrajs[self.trajId]) - ((self.motor.offset + 180)%360)*4096/360.0
#                 self.motor.goal_position = finalValue*360.0/4096.0 - ((self.motor.offset + 180)%360)
                if (self.motor.id == 4) :
                    print "traj ", self.trajId, "/", len(self.listOfTrajs)
                
                if (len(self.listOfTorqueTrajs) > 0):
                    utils.continueTorqueTraj1ToMotor(self.motor, self.listOfTorqueTrajs[self.trajId])
                
                self.trajId = (self.trajId + 1)%len(self.listOfTrajs)
                if (self.repeat == False and self.trajId == 0) :
                #We've already sent all the trajs, it's over now
                    self.over = True
    
    def __repr__(self):
        return "listOfDurations : " + str(self.listOfDurations) + "\n listOfTrajs : " + str(self.listOfTrajs) + "\n"

def evalPoly(x, coefs):
    result = coefs[0] + coefs[1]*x + coefs[2]*x**2 + coefs[3]*x**3 + coefs[4]*x**4 
    return result