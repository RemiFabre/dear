
import ik
import dk
import time
from subprocess import Popen, PIPE
import trajectoryRecorder
import numpy

class Parameters() :
    def __init__(self, robot, z=0, freq=50, method="constantSpeed", maxAccel=6000, maxSpeed=500) :
#         self.dxlIo = dxlIo
        self.robot = robot
        self.z = z
        self.freq = freq
        self.method = method
        self.maxAccel = maxAccel
        self.maxSpeed = maxSpeed
        self.id1 = 1
        self.id2 = 2
        self.id3 = 3
        self.id4 = 4
        self.ids = [self.id1, self.id2, self.id3, self.id4]
        

def setPositionToArm(x, y, z, alpha, params) :
    print "Not implemented yet, returning ..."
    return
    print "Going to x = ", x, ", y = ", y, ", z = ", z, ", alpha = ", alpha
    angles = ik.computeIK(x, y, z, alpha)
#     print "angles = ", angles
    params.robot.motor_1.goal_position = angles[0]
    params.robot.motor_2.goal_position = angles[1]
    params.robot.motor_3.goal_position = angles[2]
    params.robot.motor_4.goal_position = angles[3]
    position = getDK(params)
    print position[0], " ", position[1], " ", position[2]
    

#Doesn't return until finished
def setAnglesToArmSmooth(angles, params, timeToDestination=3) :
    print "Going smoothly to angles = ", angles
    deltas = []
    initial = []
    i = 0
    for m in params.robot.motors :
        deltas.append(angles[i] - m.present_position)
        initial.append(m.present_position)
        i = i + 1
    
    for i in range(len(deltas)) :
        deltas[i] = deltas[i]%360
        if (deltas[i] > 180) :
            deltas[i] = -360 + deltas[i]
    
    print "Duration = ", timeToDestination, " secs"
    writingPeriod = 1.0/params.freq
    elapsedTime = 0
    while elapsedTime < timeToDestination :
        progress = elapsedTime / timeToDestination
        
        i = 0
        for m in params.robot.motors :
            m.goal_position = initial[i] + progress * deltas[i]
            i = i + 1
            

        elapsedTime = elapsedTime + writingPeriod
        time.sleep(writingPeriod)
        
    print "Done !"

def getDK(params, printIt=False):
    angles = []
    angles.append(params.robot.motor_1.present_position)
    angles.append(params.robot.motor_2.present_position)
    angles.append(params.robot.motor_3.present_position)
    angles.append(params.robot.motor_4.present_position)
    angles.append(params.robot.motor_5.present_position)
    angles.append(params.robot.motor_6.present_position)
    
    return getDKFromAngles(angles, printIt)

def getDKFromAngles(angles, printIt=False):
    position = dk.computeDK(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5], printIt=printIt)
    return position

def getIK(x, y, z, alpha, printIt=False):
    print "Not implemented yet, returning ..."
    return
    angles = ik.computeIK(x, y, z, alpha)
    print "IK of [", x, ", ", y, ", ", z, ", ", alpha, "] = ", angles
    
    return angles

def setAnglesToArm(angles, params) :
    i = 0
    for m in params.robot.motors :
        m.goal_position = angles[i]
        i = i + 1

def setAngleToMotor(id, angle, params, lowLimit=-180, highLimit=180) :
    errorCounter = 0
    if (lowLimit < highLimit) :
        #Inner values
        if (angle < lowLimit) :
            angle = lowLimit
        elif (angle > highLimit):
            angle = highLimit
    else :
        #Outter values (actually works only if lowLimit is positive and highLimit is negative)
        if (angle > 0) :
            if (angle < lowLimit) :
                angle = lowLimit
        else :
            if (angle > highLimit) :
                angle = highLimit

    # print "setting true angle ", angle, " to motor ", id
    while True :
        try:
            params.dxlIo.set_goal_position({id : angle})
            break
        except:
            errorCounter = errorCounter + 1
            # print "Nope :/"
            if errorCounter > 39 :
                print "Nb errors too high : ", errorCounter
                break
            pass

### Duration in tenth of ms (10000 = 1 sec)
def setTraj1ToMotor(motor, polyCoeffs, duration, copyNextBuffer=False, mode=3) :
    try:
        #Transmitting the 5 coeffs :
        motor.duration1 = duration
        motor.traj1_size = 5
        motor.a0_traj1 = polyCoeffs[0]
        motor.a1_traj1 = polyCoeffs[1]
        motor.a2_traj1 = polyCoeffs[2]
        motor.a3_traj1 = polyCoeffs[3]
        motor.a4_traj1 = polyCoeffs[4]
        motor.mode_dynaban = mode
        motor.send_mode_delayed = True
        motor.update_buffer1 = True
        if (copyNextBuffer) :
            #Enables the copy of traj2 into traj1 once traj1 finishes
            motor.send_copy_next_buffer_write = True
            motor.copy_next_buffer_write = 1
    except:
        print "Failed to set position polynom to motor id : ", id
        
### Duration in tenth of ms (10000 = 1 sec)
def setTorqueTraj1ToMotor(motor, polyCoeffs) :
    try:
        #Transmitting the 5 coeffs :
        motor.torque1_size = 5
        motor.a0_torque1 = polyCoeffs[0]
        motor.a1_torque1 = polyCoeffs[1]
        motor.a2_torque1 = polyCoeffs[2]
        motor.a3_torque1 = polyCoeffs[3]
        motor.a4_torque1 = polyCoeffs[4]
        
        motor.update_buffer1 = True
    except:
        print "Failed to set torque polynom to motor id : ", id
        
### Duration in tenth of ms (10000 = 1 sec)
def continueTrajWithMotor(motor, polyCoeffs, duration, mode=3) :
    try:
        #Transmitting the 5 coeffs :
        motor.duration2 = duration
        motor.traj2_size = 5
        motor.a0_traj2 = polyCoeffs[0]
        motor.a1_traj2 = polyCoeffs[1]
        motor.a2_traj2 = polyCoeffs[2]
        motor.a3_traj2 = polyCoeffs[3]
        motor.a4_traj2 = polyCoeffs[4]
        motor.send_mode_delayed = True
        motor.mode_dynaban = mode
        motor.update_buffer2 = True
        #Enables the copy of traj2 into traj1 once traj1 finishes
        motor.send_copy_next_buffer_write = True
        motor.copy_next_buffer_write = 1
            
    except:
        print "Failed to set position polynom to motor id : ", id
        
def updateGoalPosition(motor):
    if (motor.mode_dynaban != 0) :
        #It would oscilate if mode == 0
        motor.goal_position = motor.present_position
    
def stopTrajectory(motor):
    print "Stopping traj for motor ", motor.id
    motor.goal_position = motor.present_position
    motor.update_buffer1 = False
    motor.update_buffer2 = False
    motor.send_copy_next_buffer_write = True
    motor.copy_next_buffer_write = 0
    motor.send_mode = True
    motor.mode_dynaban = 0
    motor.send_mode_delayed = False
        
### Duration in tenth of ms (10000 = 1 sec)
def continueTorqueTraj1ToMotor(motor, polyCoeffs) :
    try:
        #Transmitting the 5 coeffs :
        motor.torque2_size = 5
        motor.a0_torque2 = polyCoeffs[0]
        motor.a1_torque2 = polyCoeffs[1]
        motor.a2_torque2 = polyCoeffs[2]
        motor.a3_torque2 = polyCoeffs[3]
        motor.a4_torque2 = polyCoeffs[4]
        
        motor.update_buffer2 = True
    except:
        print "Failed to set torque polynom to motor id : ", id

#Outputs 2 trajRecorders. One with the trajectory coefs and the other with the torque coefs
def requestIKAndTorquesFromModel(XYZTraj, params, terribleHack=True):
        command = "../Model/build/appTestLegTorques"
#         arg = pathToXYZTraj
        arg = "gui" 
        #List of 2 trajectoryRecorders. The first one is the position trajectory, the second one is the torque trajectory
        trajs = [trajectoryRecorder.trajectoryRecorder(params.robot.motors), trajectoryRecorder.trajectoryRecorder(params.robot.motors)]
        process = Popen([command, arg], stdout=PIPE, stdin=PIPE)
        output, err = process.communicate(input=XYZTraj)
        print "Errors ? : ", err
        print "Size of output = ", len(output)
        
        if len(output) < 2 :
            print "No answer received from model ! Exiting function."
            return
#         print "Output = ", output
#         print(grep_stdout.decode())
#         (output, err) = process.communicate()
        lines = output.split('\n')
        started = False
        sumOfDurations = 0
        nbDurations = 0

        for line in lines :
            if (len(line) < 1) :
                print "Info from model read"
                break
            if started == False :
                if line.startswith("==== Torque Trajectories ====") :
                    #We're going to read the torque info
                    started = True
                    trajId = 1
                continue
            if started == True :
                if line.startswith("==== Position Trajectories ====") :
                    #We're going to read the position information
                    started = True
                    trajId = 0
                    continue
            if (line.startswith("motor")) :
                words = line.split('_')
                if len(words) != 2 :
                    raise("Expected 'motor_id', found {0}".format(line))
                id = int(words[1])
                if id == 2 or id == 3 or id == 4:
                    sign = -1
                else :
                    sign = 1
                for traj in trajs[trajId].listOfTimedTrajectories :
                    if (traj.motor.id == id) :
                        #Found our motor
#                         print "Reading coefs for motor {0}".format(id)
                        currentTraj = traj
                        currentTraj.clear
                        durationRead = False
            else :
                if durationRead == False :
                    durationRead = True
                    duration = float(line.strip())
                    currentTraj.durations.append(int(duration*10000))
                    sumOfDurations = sumOfDurations + duration
                    nbDurations = nbDurations + 1
#                     print "Duration = ", int(duration*10000)
                else :
                    durationRead = False
                    words = line.split(',')
                    for i in range(len(words)) :
                        words[i] = words[i].strip()
    
                    if (len(words) != 5) :
                        raise("Expected 'c0, c1, C2, c3, c4', found {0}".format(line))
                    listOfCoefs = []
                    for coef in words :
                        if trajId == 0 :
                            #Position conversion
                            value = float(coef)*(180.0/numpy.pi)*(4096/360.0)*sign
                            listOfCoefs.append(value)
                        elif trajId == 1 :
                            #Torque conversion
                            value = float(coef)*(4096/(2*numpy.pi))*sign
                            listOfCoefs.append(value)

                    currentTraj.listOfTrajCoefs.append(listOfCoefs)
        
        exit_code = process.wait()
        print "sumOfDurations = ", sumOfDurations/12.0
        print "nbDurations = ", nbDurations/12.0
        print "avgDuration = ", (sumOfDurations/12.0)/(nbDurations/12.0)
        if terribleHack :
            #Probably the ugliest hack of my life
            trajs[0].addBSTrajs()
            trajs[1].addBSTrajs()
            trajs[0].addBSTrajs()
            trajs[1].addBSTrajs()
        return trajs[0], trajs[1]

def requestForceFromModel(params):
    inputStream = ""
    id = 1
    sign = 1
    for m in params.robot.motors :
        if id == 2 or id == 3 or id == 4:
            sign = -1
        else :
            sign = 1
        id = id + 1
        inputStream += str(m.present_position) + " " + str(sign*m.output_torque) + "\n"    

#     print inputStream
    
    command = "../Model/build/appTestLegTorques"
    arg = "force"
    process = Popen([command, arg], stdout=PIPE, stdin=PIPE)
    result = process.communicate(input=inputStream)[0]
 
#     print "Results = \n" + result
    output = result.split('\n')
    
    return output

def requestAntiGravityFromModel(params):
    inputStream = ""
    id = 1
    sign = 1
    for m in params.robot.motors :
        if id == 2 or id == 3 or id == 4:
            sign = -1
        else :
            sign = 1
        id = id + 1
        inputStream += str(m.present_position) + " " + str(0.0) + "\n"    

#     print inputStream
    
    command = "../Model/build/appTestLegTorques"
    arg = "gravity"
    process = Popen([command, arg], stdout=PIPE, stdin=PIPE)
    result = process.communicate(input=inputStream)[0]
 
#     print "Results = \n" + result
    output = result.split('\n')
    
    return output
    
def sign(x) :
    if (x > 0) :
        return 1
    if (x < 0) :
        return -1
    return 1

def isfloat(value):
  try:
    float(value)
    return True
  except:
    return False
