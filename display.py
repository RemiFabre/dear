# -*- coding: utf-8 -*-
#Author : Rémi Fabre

import math
import matplotlib
from numpy import average
import numpy
import os
import pickle
import pygame
from pygame.locals import *
import pypot.robot
import sys
import time

import configuration
import formalCalc
import matplotlib.pyplot as plt
import motorTrajectory
import motorTrajectoryManager
import trajectory3D
import trajectoryRecorder
import utils


matplotlib.use('Agg') # TO avoid the "Tcl_AsyncDelete: async handler deleted by the wrong thread" error

def createDisplay(params) :
    #Initialisations
    pygame.init()
    DEBUG_ON = False
    nbPixelsPerMm = 3.0
    WIDTH = int(210*nbPixelsPerMm)#800
    HEIGHT = int(297*nbPixelsPerMm)#400
    black = 0,0,0
    white = 255,255,255

    #Screen creation
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    black = 0,0,0
    screen.fill(black)

    chartImage = pygame.image.load('effortChart.jpg').convert()
    chartRect = chartImage.get_rect()

    chartRect.centerx = WIDTH/2 + 3
    chartRect.centery = HEIGHT/2 + 8
    screen.blit(chartImage, chartRect)

    clock = pygame.time.Clock()


    xDrawingVertical = 200
    yDrawingVertical = 0
    zDrawingVertical = -100

    xDrawingHorizontal = 280
    yDrawingHorizontal = 0
    zDrawingHorizontal = 100
    
    initAngles = [0, 0, 137, 66, 33, 0]
    initAnglesPunition = [0, 0, 75, 32, 45, 0]
    # # 4 quarter of Sinus, 500 ms each. Amplitude = 170 (15°)
    listOfTraj =    [
                     [0.0029282637700648157, 532.49287882689293, 29.07849099701108, -1058.1470413492355, 459.3664329672057], 
                     [169.9973127875532, 1.2118904739507608, -859.49525560910968, 109.93882674890278, 489.17556618589202], 
                     [-0.0029282637700933502, -532.49287882689202, -29.078490997017791, 1058.1470413492527, -459.36643296722087], 
                     [-169.99731278755326, -1.2118904739506096, 859.49525560910888, -109.93882674889758, -489.17556618590021]
                     ]
    listOfTorques = []
#     for traj in listOfTraj :
#         tempList = []
#         for value in traj :
#             tempList.append(value * 2)
#         listOfTorques.append(tempList)
        
#     listOfTraj = [
#                   [0.0, 0.0, 0.0, 0.0, 0.0],
#                   [0.0, 0.0, 0.0, 0.0, 0.0],
#                   [0.0, 0.0, 0.0, 0.0, 0.0],
#                   [0.0, 0.0, 0.0, 0.0, 0.0],
#                   ]

    currentTrajId1 = 0

    currentTime = 0
    oldTime = 0
    t0 = 0
    sign = 1
    
    mode = "init"
    initMode = "OFF"
    knifeMode = "OFF"
    drawingMode = "OFF"
    recordingMode = "OFF"
    replayMode = "OFF"
    DKMode = "OFF"
    requestForceMode = "OFF"
    requestAntiGravityFromModel = "OFF"
    serializedFileName = "victoryMove.p"
    torqueCoefsFileName = "torqueCoefsFile.t"
    trajectoryFileName = "trajectoryFile.t"
    drawingFileName = 'recordedDrawing.rec'
    specialFilePath = "font2/special.dat"
    startOfReplay = False
    
    firstTraj = True
    idMotor = 1
    nbTrajSent = 0
    
    trajMan =  motorTrajectoryManager.MotorTrajectoryManager()
    trajMan.createMotorTrajectory(params.robot.motors[0], listOfTraj, listOfTorques, [5000, 5000, 5000, 5000], mode=2, repeat=True, delay=0.1)
    trajMan.createMotorTrajectory(params.robot.motors[1], listOfTraj, listOfTorques, [5000, 5000, 5000, 5000], mode=2, repeat=True, delay=0.1)
#     trajMan.createMotorTrajectory(params.robot.motors[2], listOfTraj, listOfTorques, [5000, 5000, 5000, 5000], mode=2, repeat=True, delay=0.1)
    trajMan.createMotorTrajectory(params.robot.motors[3], listOfTraj, listOfTorques, [5000, 5000, 5000, 5000], mode=2, repeat=True, delay=0.1)
    trajMan.createMotorTrajectory(params.robot.motors[4], listOfTraj, listOfTorques, [5000, 5000, 5000, 5000], mode=2, repeat=True, delay=0.1)
    trajMan.createMotorTrajectory(params.robot.motors[5], listOfTraj, listOfTorques, [5000, 5000, 5000, 5000], mode=2, repeat=True, delay=0.1)

    #If I don't put the '[]' below, trajReplay is created with a list identical to the list of trajMan. What did I miss?
    trajReplay =  motorTrajectoryManager.MotorTrajectoryManager([])

    trajRecorder = trajectoryRecorder.trajectoryRecorder(params.robot.motors)
    torqueRecorder = trajectoryRecorder.trajectoryRecorder(params.robot.motors)
    trajRecorderForReplay = trajectoryRecorder.trajectoryRecorder(params.robot.motors)
    compensationTorquesPerMotor = []
    zForce = []
    zForceT = []
    tempZForceAverage = 0.0
    firstForceRequest = True
    
    initialLetterOffsetX = 0.12#0.225 
    trueX = initialLetterOffsetX
    initialLetterOffsetY = 0.14
    letterDeltaX = 0.03
    
    while(1):
        clock.tick(100) # => the loop lasts at least 1/100s
        #Tick doesn't seem to be working
        pygame.display.update()

        if (DKMode == "ON") :
            utils.getDK(params, printIt=True)
        if (mode == "drawingHorizontal" and drawingMode == "SINUS") :
            temp = 22 #Not implemented yet
#             currentTime = time.time() - t0
#             z = zDrawingHorizontal - 50*math.sin(currentTime)
#             y = yDrawingHorizontal - 50*math.cos(currentTime)
#             x = xDrawingHorizontal
#             alpha = 0
#             utils.setPositionToArm(x, y, z, alpha, params)
        elif (mode == "drawingVertical" and drawingMode == "SINUS") :
            temp = 22 #Not implemented yet
#             currentTime = time.time() - t0
#             x = xDrawingVertical - 50*math.sin(currentTime)
#             y = yDrawingVertical - 50*math.cos(currentTime)
#             z = zDrawingVertical
#             alpha = -90
#             utils.setPositionToArm(x, y, z, alpha, params)
        elif (mode == "eventail" and drawingMode == "ON") :
            currentTime = time.time() - t0
            angles = list(initAngles)
            angles[2] = angles[2] + 8*math.sin(currentTime*6)
            angles[3] = angles[3] + 16*math.sin(currentTime*6)
            angles[4] = angles[4] - 24*math.sin(currentTime*6)
            utils.setAnglesToArm(angles, params)
        elif (mode == "punition" and drawingMode == "ON") :
            currentTime = time.time() - t0
            angles = list(initAnglesPunition)
            angles[2] = angles[2] + 15*math.sin(currentTime*6)
            angles[3] = angles[3] + 13*math.sin(currentTime*6)
            angles[4] = angles[4] - 10*math.sin(currentTime*6)
            utils.setAnglesToArm(angles, params)
        elif (mode == "trajectory" and drawingMode == "ON") :
            if (firstTraj) :
                trajMan.start()
                firstTraj = False
            else :
                trajMan.tick()   
        elif (mode == "recording" and recordingMode == "ON") :
            currentTime = time.time() - t0
            trajRecorder.tick(currentTime)
        elif (mode == "replay" and replayMode == "ON") :
            #Problem : if we start recording as soon as we start replaying, we have a delay on the recording. 
            #Between the moment we asked for a traj to be followed, and the moment the servo starts playing it, there is usually 2 ticks (~0.04 s). 
            #1 tick beause this loop is asynchrounous, and 1 tick because of the delayed mode feature.
            #Solution : we'll check the moment at which the motors received the mode change.
            currentTime = time.time() - t0
            if (startOfReplay) :
                if (currentTime >= 0.02) :
                    for t in trajReplay.listOfMotorTrajs :
                        if (t.motor.send_mode_delayed == False and t.motor.send_mode == False) :
                            #This means at least a motor will start playing the traj
                            startOfReplay = False
                            timeOffset = currentTime
                            break
            else :
                #We can start recording now :
                trajRecorderForReplay.tick(currentTime - timeOffset)
            #0, 1 and 2 are the torques. 4, 5, and 6 are the force through x, y, and z at the end of the pen.    
            tempZForce = float(utils.requestForceFromModel(params)[5])
            tempZForceAverage = movingAverage(tempZForceAverage, tempZForce, 10, 20000, -20000)
            zForce.append(tempZForceAverage)
            zForceT.append(currentTime)
            if (firstTraj) :
                trajReplay.start()
                firstTraj = False
            else :
                if trajReplay.tick() == False :    
                    #End of replay
                    pass
        if (requestForceMode == "ON") :
            if firstForceRequest :
                tempZForceAverage = 0.0
                firstForceRequest = False
                graphSize = 100
                setBack = 30
                plt.ion()
                #Set up plot
                figure, ax = plt.subplots(2)
                xForce = []
                yForce = []
                yForceAverage = []
                lines,  = ax[0].plot(xForce, yForce, '--')
                lines2,  = ax[1].plot(xForce, yForceAverage, '--')
                #Autoscale on unknown axis and known lims on the other
#                 ax[0].set_autoscaley_on(True)
                ax[0].set_xlim(0, 30)
                ax[0].set_ylim(-10000, 10000)
                ax[0].set_autoscalex_on(True)
                ax[1].set_xlim(0, 30)
                ax[1].set_ylim(-10000, 10000)
                ax[1].set_autoscalex_on(True)
                #Other stuff
                ax[0].grid()
                ax[1].grid()
                t0 = time.time()
            else :
                currentTime = time.time() - t0
                tempZForce = float(utils.requestForceFromModel(params)[5])
                tempZForceAverage = movingAverage(tempZForceAverage, tempZForce, 10, 20000, -20000)
#                 print "tempZForce = ", tempZForce
                t = time.time() - t0
                xForce.append(t)
                yForce.append(tempZForce)
                yForceAverage.append(tempZForceAverage)
                
                if (len(xForce) > graphSize) :
                    xForce = xForce[setBack:graphSize]
                    yForce = yForce[setBack:graphSize]
                    yForceAverage = yForceAverage[setBack:graphSize]
                #Update data (with the new _and_ the old points)
                lines.set_xdata(xForce)
                lines.set_ydata(yForce)
                lines2.set_xdata(xForce)
                lines2.set_ydata(yForceAverage)
    
                #Need both of these in order to rescale
                ax[0].relim()
                ax[0].autoscale_view()
                ax[1].relim()
                ax[1].autoscale_view()
                #We need to draw *and* flush
                figure.canvas.draw()
                figure.canvas.flush_events()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                # sys.exit()
                    
            if (mode == "init") :
                if event.type == KEYDOWN:
                    print "Keydown in init !"
                    if event.key == K_TAB:
                        mode = "recording"
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (initMode == "OFF") :
                            initMode = "ON1"
                            utils.setAnglesToArmSmooth([0, 0, 0, 0, 0, 0], params, timeToDestination = 6)
                        elif (initMode == "ON1") :
                            initMode = "ON2"
                            utils.setAnglesToArmSmooth(initAngles, params) 
                        elif (initMode == "ON2") :
                            initMode = "ON3"
                            utils.setAnglesToArmSmooth(initAnglesPunition, params) 
                        elif (initMode == "ON3") :
                            initMode = "OFF"
                        
                        print "initMode : " + str(initMode)
            elif (mode == "recording") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "replay"
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (recordingMode == "OFF") :
                            recordingMode = "init"
                            trajRecorder.clear()
                            print "Releasing everything"
                            for m in params.robot.motors :
                                m.compliant = True
#                             utils.setAnglesToArmSmooth([0, 0, 0, 0, 0, 0], params, 2)
#                             utils.setAnglesToArmSmooth([0, 0, 147, 92, 59, 0], params, 2)
#                             utils.setAnglesToArmSmooth([90.0, 0.0, -28.349999999999941, 319.34750682591192, -24.419999999999973, 0.0], params, 2)
                        elif (recordingMode == "init") :
                            recordingMode = "releasing servos"
                            for m in params.robot.motors :
                                m.compliant = True
                        elif (recordingMode == "releasing servos") :
                            recordingMode = "ON"
                            t0 = time.time()
                        elif (recordingMode == "ON") :
                            recordingMode = "OFF"
                            for m in params.robot.motors :
                                m.compliant = False
                            print "Calculating polynomial approximation"
                            for t in trajRecorder.listOfTimedTrajectories :
                                t.approximateTraj(nbPointsPerPoly=25, polyDegree=4, vipCoeff=4, vipLength=3)
#                             print trajRecorder
                            print "Saving move ..."
                            trajRecorder.save(serializedFileName)
                            
                        print "recordingMode : " + str(recordingMode)
            elif (mode == "replay") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "eventail"
                        print "Mode : " + str(mode)
                    if (event.key == K_l) :
                        print "Loading serialized file '", serializedFileName, "' ..." 
#                         trajRecorder = pickle.load( open(serializedFileName, "rb" ) )
                        trajRecorder = trajRecorder.load(serializedFileName, params.robot.motors)
                        for t in trajRecorder.listOfTimedTrajectories :
                            if (t.durations[-1] == 0) :
                                #Old bug solved but still present in the old serialized files
                                t.durations = t.durations[:-1]
                                t.listOfTrajCoefs = t.listOfTrajCoefs[:-1]
#                         #Temp !!!!!!!!!!!!!!!!!!!!!!!!
#                         print "Size of listOfTimedTrajectories = ", len(trajRecorder.listOfTimedTrajectories)
#                         for t in trajRecorder.listOfTimedTrajectories :
#                                 t.listOfTimedPositions = list(t.listOfTimedPositions[:])
#                         print "Size of listOfTimedTrajectories = ", len(trajRecorder.listOfTimedTrajectories)
# 
#                         print "If you see this message, then I forgot to erase a dangerous temp line !!"
#                         for t in trajRecorder.listOfTimedTrajectories :
#                             t.approximateTraj(nbPointsPerPoly=25, polyDegree=4, vipCoeff=100, vipLength=1)

#                         while True :
#                             pass

                        print "Loaded !"
                    if (event.key == K_t) :
                        print "Loading file containing torque coefs : '", torqueCoefsFileName, "' ..." 
                        torqueRecorder.readFromFile(torqueCoefsFileName, fromLephTorque=True)
                        print "Loaded !"
                        
                        print "Loading file containing traj coefs : '", trajectoryFileName, "' ..." 
                        trajRecorder.readFromFile(trajectoryFileName, fromLeph=True)

                        print "Loaded !"
                        
#                         TO DO : do better than this :
                        for traj in torqueRecorder.listOfTimedTrajectories :
                            compensationTorquesPerMotor.append(traj.listOfTrajCoefs)
                    if (event.key == K_a) :
#                         XYZFile = "./prettyFunction1.rec"
                        #Y ranges : [-5 to -25]
                        #X ranges = [7 to 19]
                        # Working square is X = [7, 14], Y = [-5, -19]
                        X, Y = formalCalc.prettyFunction1(xOffset=13, yOffset=-8)#(xOffset=13, yOffset=-13)
                        XYZFile = formalCalc.writeTrajectory(X, Y, z=0.017, dt=0.0125, nbPointsPerPoly=25, toString=True)
                        print "XYZFile = \n", XYZFile
                        print "Requesting IK and torques from model ..."
                        trajRecorder, torqueRecorder = utils.requestIKAndTorquesFromModel(XYZFile, params)
                        
#                         TO DO : do better than this :
                        for traj in torqueRecorder.listOfTimedTrajectories :
                            compensationTorquesPerMotor.append(traj.listOfTrajCoefs)
                    if (event.key == K_f) :
                        if (requestForceMode == "ON") :
                            requestForceMode = "OFF"
                        elif (requestForceMode == "OFF") :
                            requestForceMode = "ON"
                            firstForceRequest = True
                    if (event.key == K_g) :
                        if (requestAntiGravityFromModel == "ON") :
                            requestAntiGravityFromModel = "OFF"
                        elif (requestAntiGravityFromModel == "OFF") :
                            requestAntiGravityFromModel = "ON"
                            requestAntiGravityFromModel = True
                    if (event.key == K_x) :              
                        traj3D = trajectory3D.Trajectory3D()
                        traj3D.readFromXYFile("logoRhoban.dat", z=0.007, dt=0.08, scale=1.0, offsetX=0.1, offsetY=-0.1)
 
                        traj3D.approximateTraj(nbPointsPerPoly=15, polyDegree=4, vipCoeff=4, vipLength=3)
                        XYZFile = traj3D.saveToString()
                        print "XYZFile = \n", XYZFile
                        print "Requesting IK and torques from model ..."
                        trajRecorder, torqueRecorder = utils.requestIKAndTorquesFromModel(XYZFile, params)
                         
#                         TO DO : do better than this :
                        for traj in torqueRecorder.listOfTimedTrajectories :
                            compensationTorquesPerMotor.append(traj.listOfTrajCoefs)
                            
                    if (event.key == K_o) :              
                        traj3D = trajectory3D.Trajectory3D()
                        sentence = raw_input("Which sentence to draw ?")
                        letters = list(sentence)
                        first = True
                        isLastLetter = False
                        index = 0
                        for letter in letters :
                            print "\n>>>>>>>>>>>>>> Processing letter : ", letter
                            if (letter == ' ') :
                                print "Space !"
                                initialLetterOffsetX = initialLetterOffsetX - 0.015
                                continue
                            if (letter == '_') :
                                print "break line !"
                                initialLetterOffsetY = initialLetterOffsetY - 0.03
                                continue
                            if (index == (len(letters) -1)) :
                                isLastLetter = True
                            XYZFile=""
                            path = "font2/" + str(letter) + ".dat"

                            scale = 6.0
                            xAnchor, yAnchor, xMax, xMin, yMax, yMin =  traj3D.readInfoFromLetterFile(path, scale=scale)
                            print xAnchor, yAnchor, xMax, xMin, yMax, yMin
                            width = xMax - xMin
                            height = yMax - yMin
                            xMoy = (xMax + xMin) / 2.0
                            traj3D = trajectory3D.Trajectory3D()
                            traj3D.readFromXYFile(path, z=0.0105, dt=0.03, scale=scale, offsetX=initialLetterOffsetX, offsetY=initialLetterOffsetY, isLetter=True, xAnchor=xAnchor, yAnchor=yAnchor, xMoy=xMoy)
                            initialLetterOffsetX = initialLetterOffsetX - width
     
                            traj3D.approximateTraj(nbPointsPerPoly=20, polyDegree=4, vipCoeff=4, vipLength=3)
                            print "Approx went fine."
                            XYZFile = traj3D.saveToString()
                            print "Requesting IK and torques from model ..."
                            if (first) :
                                trajRecorder, torqueRecorder = utils.requestIKAndTorquesFromModel(XYZFile, params, terribleHack=False)
                                first = False
                            else :
                                tempTrajRecorder, tempTorqueRecorder = utils.requestIKAndTorquesFromModel(XYZFile, params, terribleHack=isLastLetter)
                                print "nbPosTrajs = ", len(tempTrajRecorder.listOfTimedTrajectories[4].listOfTrajCoefs)
                                print "nbTorqueTrajs = ", len(tempTorqueRecorder.listOfTimedTrajectories[4].listOfTrajCoefs)
                                trajRecorder.appendTrajectoryRecorder(tempTrajRecorder)
                                torqueRecorder.appendTrajectoryRecorder(tempTorqueRecorder)
                                
                            print "-> total nbPosTrajs = ", len(trajRecorder.listOfTimedTrajectories[4].listOfTrajCoefs)
                            print "-> total nbTorqueTrajs = ", len(torqueRecorder.listOfTimedTrajectories[4].listOfTrajCoefs)
                            
#                         TO DO : do better than this :
                        for traj in torqueRecorder.listOfTimedTrajectories :
                                compensationTorquesPerMotor.append(traj.listOfTrajCoefs)
                        print "We're ready to write !"
                    if (event.key == K_p) :              
                        traj3D = trajectory3D.Trajectory3D()
                        sentence = raw_input("(special) Which sentence to draw ?")
                        letters = list(sentence)
                        first = True
                        isLastLetter = False
                        specialFile = open(specialFilePath,'w+')
                        for letter in letters :
                            print "\nLetter : ", letter
                            if (letter == ' ') :
                                print "Space !"
                                initialLetterOffsetX = initialLetterOffsetX - 0.015
                                continue
                            if (letter == '_') :
                                print "break line !"
                                initialLetterOffsetY = initialLetterOffsetY - 0.045
                                initialLetterOffsetX = trueX
                                continue
                            
                            path = "font2/" + str(letter) + ".dat"
                            scale = 5.0
                            xAnchor, yAnchor, xMax, xMin, yMax, yMin =  traj3D.readInfoFromLetterFile(path, scale=scale)
                            print xAnchor, yAnchor, xMax, xMin, yMax, yMin
                            width = xMax - xMin
                            height = yMax - yMin
                            xMoy = (xMax + xMin) / 2.0
                            yMoy = (yMax + yMin) / 2.0
                            initialLetterOffsetX = initialLetterOffsetX - width
                            addFileToSpecialFile(path, specialFile, scale, initialLetterOffsetX, initialLetterOffsetY, xAnchor, yAnchor, xMoy, yMoy)
                        print "Optimizing specialFile ..."
                        #4mm in scale 5 is okay
                        newSpecialFile, nbOptimizations = optimizeSpecialFile(specialFile, 0.001*20/float(scale))
                        specialFile.close()
                        if (nbOptimizations > 0) :
                            #We're rewritting this with the new output
                            specialFile = open(specialFilePath,'w+')
                            specialFile.write(newSpecialFile)
                            specialFile.close()
                        print "SpecialFile done !"
                        
                        traj3D = trajectory3D.Trajectory3D()
                        traj3D.readFromXYFile("font2/special.dat", z=0.077, dt=0.008)
                        
                        traj3D.approximateTraj(nbPointsPerPoly=25, polyDegree=4, vipCoeff=4, vipLength=3)
                        print "Approx went fine."
                        XYZFile = traj3D.saveToString()
                        print "XYZ :\n", XYZFile
                        print "Requesting IK and torques from model ..."
                        trajRecorder, torqueRecorder = utils.requestIKAndTorquesFromModel(XYZFile, params, terribleHack=True)
#                         TO DO : do better than this :
                        for traj in torqueRecorder.listOfTimedTrajectories :
                                compensationTorquesPerMotor.append(traj.listOfTrajCoefs)
                                                        
                    if (event.key == K_RETURN) :
                        if (replayMode == "OFF") :
                            replayMode = "init"
                            tempAngles = []
                            for t in trajRecorder.listOfTimedTrajectories :
                                #The first coef is the initial position
                                value = t.listOfTrajCoefs[0][0]
                                tempAngles.append(value*360/4096.0)
                            utils.setAnglesToArmSmooth(tempAngles, params, 2)
                        elif (replayMode == "init") :
                            startOfReplay = True
                            replayMode = "ON"
                            trajRecorderForReplay.clear()
                            trajReplay.clear()
                            t0 = time.time()
                            trajRecorderForReplay.clear()
                            trajIndex = 0
                            for t in trajRecorder.listOfTimedTrajectories :
                                if (len(compensationTorquesPerMotor) < 1) :
                                    torqueCoefs = []
                                else :
                                    torqueCoefs = compensationTorquesPerMotor[trajIndex]
                                trajReplay.createMotorTrajectory(t.motor, t.listOfTrajCoefs, torqueCoefs, t.durations, mode=3, repeat=False, delay=0.1)
                                trajIndex = trajIndex + 1

                            firstTraj = True
                        elif (replayMode == "ON") :
                            replayMode = "OFF"
                            #Making sure all the trajectories are stopped
                            trajReplay.stopAll()
#                             print trajRecorderForReplay
                            print "Saving actual trajectory"
                            trajRecorderForReplay.save("actualTrajectory.p")

#                             print "Ploting actual trajectory ..."
#                             for t in trajRecorderForReplay.listOfTimedTrajectories :
#                                 if (t.motor.id == 4) :
#                                     t.plot("*") 
                            print "Trajectory is over :)"
                            if (DEBUG_ON == False) :
                                continue
                            print "Plotting the Z force "
                            plt.plot(zForceT, zForce, "--")
                            plt.legend(['Z force']) #'sent torque'])
                            plt.grid(True)
                            plt.show()
                            
                            print "Calculating delta between the initial recording and its polynomial approximation"
                            deltaApprox = trajRecorder.delta(trajRecorder, params.robot.motors)
                            
                            print "Calculating delta between what was wanted and what was done"
                            deltaTraj = trajRecorder.delta(trajRecorderForReplay, params.robot.motors)
                            
                            for c, m, p, d in zip(trajRecorder.listOfTimedTrajectories, trajRecorderForReplay.listOfTimedTrajectories, 
                                                  deltaApprox.listOfTimedTrajectories, deltaTraj.listOfTimedTrajectories) : #torqueRecorder.listOfTimedTrajectories
                                if (c.motor.id <= 10) :
                                    cPlot = c.getPlottable()
                                    aPlot = c.getPlottableFromPolyCoefs()
                                    mPlot= m.getPlottable()
                                    pPlot = p.getPlottable()
                                    dPlot = d.getPlottable()
#                                     tPlot = t.getPlottableFromPolyCoefs()
                                    print "Plotting ..."
                                    tempc = [(x) for x in cPlot[0]]
                                    tempa = [(x) for x in aPlot[0]]
                                    
                                    plt.plot(tempc, cPlot[1], "*", 
                                             tempa, aPlot[1], "+", 
                                             mPlot[0], mPlot[1], "x", 
                                             pPlot[0], pPlot[1], "--", 
                                             dPlot[0], dPlot[1], "-.")
#                                              tPlot[0], tPlot[1], "--")
                                    plt.legend(['Ideal', 'Poly approx', 'Measured', 'Approx - Ideal', 'Approx - Measured']) #'sent torque'])
                                    plt.grid(True)
                                    plt.show()
                            
                        print "replayMode : " + str(replayMode)
            elif (mode == "eventail") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "punition"
                        firstTraj = True
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (drawingMode == "OFF") :
                            drawingMode = "ON"
                            t0 = time.time()
                        elif (drawingMode == "ON") :
                            drawingMode = "OFF"
                        print "drawingMode : " + str(drawingMode)
            elif (mode == "punition") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "trajectory"
                        firstTraj = True
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (drawingMode == "OFF") :
                            drawingMode = "ON"
                            t0 = time.time()
                        elif (drawingMode == "ON") :
                            drawingMode = "OFF"
                        print "drawingMode : " + str(drawingMode)
            elif (mode == "trajectory") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "drawingRecording"
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (drawingMode == "OFF") :
                            drawingMode = "ON"
                            t0 = time.time()
                        elif (drawingMode == "ON") :
                            drawingMode = "OFF"
                        print "drawingMode : " + str(drawingMode)
                  
            elif (mode == "drawingRecording") :
#                 time.sleep(0.02)
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "drawingHorizontal"
                        x = xDrawingHorizontal
                        y = yDrawingHorizontal
                        z = zDrawingHorizontal
                        alpha = 0
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (drawingMode == "OFF") :
                            drawingMode = "ON"
                            os.remove(drawingFileName)
                            f = open(drawingFileName,'w')
                            traj3D = trajectory3D.Trajectory3D()
                            startedRecording = False
                        elif (drawingMode == "ON") :
                            drawingMode = "OFF"
                            traj3D.approximateTraj(nbPointsPerPoly=50, polyDegree=4, vipCoeff=4, vipLength=7)
                            traj3D.saveToFile("recordingDrawingCoefs.rec")
                            plottable = traj3D.getPlottable()
                            plt.plot(plottable[0], plottable[1], "x", 
                                             plottable[0], plottable[2], "--", 
                                             plottable[0], plottable[3], ",")
#                                              tPlot[0], tPlot[1], "--")
                            plt.legend(['x', 'y', 'z']) #'sent torque'])
                            plt.grid(True)
                            plt.show()
                        print "drawingMode : " + str(drawingMode)
                if (drawingMode == "ON") :
                    if (pygame.mouse.get_pressed()[0] == 1 and startedRecording == False) :
                    # [0] for left click, [2] for right click
                        startedRecording = True
                        t0 = time.time()
                    if startedRecording :
                        xMouse, yMouse = pygame.mouse.get_pos()
                        #In meters
                        x = ((HEIGHT - yMouse)/nbPixelsPerMm + 100)/1000.0
                        y = ((xMouse - WIDTH/2)/nbPixelsPerMm)/1000.0
#                         if (pygame.mouse.get_pressed()[0]) :
#                             z = 0
#                         else :
#                             z = max(-0.01, z - 0.0004)
                        z = 0
                        t = time.time() - t0
                        traj3D.listOfTimedPositions.append([t, x, y, z])
                        line = str(t) + ' ' + str(x) + ' ' + str(y) + '\n'
                        print line
                        f.write(line)

            elif (mode == "drawingHorizontal") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "DK"
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        if (drawingMode == "OFF") :
                            drawingMode = "ON"
                            x = xDrawingHorizontal
                            y = yDrawingHorizontal
                            z = zDrawingHorizontal
                            alpha = 0
                        elif (drawingMode == "ON") :
                            drawingMode = "SINUS"
                            t0 = time.time()
                        elif (drawingMode == "SINUS") :
                            drawingMode = "OFF"
                            x = xDrawingHorizontal
                            y = yDrawingHorizontal
                            z = zDrawingHorizontal
                            alpha = 0
                            utils.setPositionToArm(x, y, z, alpha, params)
                        print "drawingMode : " + str(drawingMode)
                if (drawingMode == "ON") :
                    if (event.type == pygame.MOUSEMOTION or event.type == pygame.MOUSEBUTTONDOWN) :
                        xMouse, yMouse = pygame.mouse.get_pos()
                        z = zDrawingHorizontal - int((yMouse - WIDTH/2)*0.2)
                        y = yDrawingHorizontal - int((xMouse - HEIGHT/2)*0.2)

                    utils.setPositionToArm(x, y, z, alpha, params)
            elif (mode == "DK") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "IK"
                        print "Mode : " + str(mode)
                    if event.key == K_d:
                        #Disables torque
                        for m in params.robot.motors :
                            m.compliant = True
                    if event.key == K_e:
                        #Enables torque
                        for m in params.robot.motors :
                            m.compliant = False
                    if (event.key == K_RETURN) :
                        if (DKMode == "OFF") :
                            DKMode = "ON"
                        else :
                            DKMode = "OFF"
            elif (mode == "IK") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "pidConfiguration"
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        while True :
                            print "Give a position (x enter y enter z enter alpha enter): "
                            xTemp = float(raw_input("X = "))
                            yTemp = float(raw_input("Y = "))
                            zTemp = float(raw_input("Z = "))
                            alphaTemp = float(raw_input("alpha = "))
                            
                            angles = utils.getIK(xTemp, yTemp, zTemp, alphaTemp, printIt=True)
                            utils.getDKFromAngles(angles, printIt=True)

            elif (mode == "pidConfiguration") :
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "init"
                        print "Mode : " + str(mode)
                    if (event.key == K_RETURN) :
                        print "Current PID values : "
                        for m in params.robot.motors:
                            print m.pid
                            
                        P = raw_input("Enter P coeff : ")
                        if (utils.isfloat(P)) :
                            P = float(P)
                        else :
                            mode = "init"
                            print "Mode : " + str(mode) 
                            continue
                        I = raw_input("Enter I coeff : ")
                        if (utils.isfloat(I)) :
                            I = float(I)
                        else :
                            mode = "init"
                            print "Mode : " + str(mode)
                            continue
                        D = raw_input("Enter D coeff : ")
                        if (utils.isfloat(D)) :
                            D = float(D)
                        else :
                            mode = "init"
                            print "Mode : " + str(mode)
                            continue
                        for m in params.robot.motors:
                            m.pid = [P, I, D]
                            print m.pid
                        mode = "init"
                        print "Mode : " + str(mode)
                        

#Moving average with an outlier deletion
def movingAverage(average, newValue, coef, maxValue, minValue):
    if newValue> maxValue :
        return average
    elif newValue < minValue :
        return average
    else :
        return (coef * average + newValue) / float(coef + 1)

def addFileToSpecialFile(path, specialFile, scale, offsetX, offsetY, xAnchor, yAnchor, xMoy, yMoy):
    print "offsetY = ", offsetY, "yAnchor = ", yAnchor, "yMoy = ", yMoy
    f = open(path, 'r')
    index = 0
    offsetX = offsetX
    offsetY = offsetY - yAnchor

    for line in f :
        if (line.startswith("U")) :
            specialFile.write(line)
        elif (line.startswith("Anchor")) :
            continue
        else :
            words = line.split(" ")         
            x = (scale*float(words[0])/40000.0 + offsetX)*(-1) + 2*(xMoy + offsetX) #Axial symetry
            y = scale*-1*float(words[1])/40000.0 + offsetY
            specialFile.write(str(x) + " " + str(y) + "\n")
        index = index + 1
        
    print "Wrote nb lines = ", index
    
def optimizeSpecialFile(specialFile, maxDistance):
    previousWasU = False
    output = ""
    x = 0.0
    y = 0.0
    pX = 0.0
    pY = 0.0
    nbOptimizations = 0
    specialFile.seek(0,0)
    for line in specialFile :
        if (line.startswith("U")) :
            if previousWasU :
                #We're not writting 2 U on a row
                pass
            else :
                previousWasU = True
                output = output + line
        else :
            words = line.split(" ")
            pX = x 
            pY= y
            x = float(words[0])
            y = float(words[1])
            if previousWasU :
                distance = numpy.sqrt( (x - pX)*(x - pX) + (y - pY)*(y - pY) )
                print "Distance = ", distance
                if (distance <= maxDistance) :
                    #The 2 points are close enough, no need to lift the pen for that
                    output = output[:-2]
                    nbOptimizations = nbOptimizations + 1
            output = output + str(x) + " " + str(y) + "\n"
            previousWasU = False

    print nbOptimizations," optimizations"
    return output, nbOptimizations
        