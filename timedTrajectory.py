import numpy
import matplotlib.pyplot as plt
from operator import truediv

class TimedTrajectory():
    def __init__(self, motor) :
        self.motor = motor
        self.listOfTimedPositions = []# A timed position respects the format : [time, position]
        self.listOfTrajCoefs = []
        self.durations = []
        self.listOfWeights = []
        self.dynamicOffset = 0
        
    def update(self, time) :
        self.motor.updated = False
        position = self.motor.present_position + self.dynamicOffset
        if (len(self.listOfTimedPositions) > 0) :
            delta = position - self.listOfTimedPositions[-1][1]
            # Did we went from 0 <-to-> 360 or from -180 <-to-> 180
            if (delta > 180) :
                self.dynamicOffset = self.dynamicOffset - 360
                position = self.motor.present_position + self.dynamicOffset
            elif (delta < -180) :
                self.dynamicOffset = self.dynamicOffset + 360
                position = self.motor.present_position + self.dynamicOffset
                 
        self.listOfTimedPositions.append([time, position])
        
    def clear(self):
        self.listOfTimedPositions = []
        self.listOfTrajCoefs = []
        self.durations = []
    
    def appendTraj(self, newTraj):
        for t in newTraj.listOfTimedPositions :
            self.listOfTimedPositions.append(t)
        for t in newTraj.listOfTrajCoefs :
            self.listOfTrajCoefs.append(t)
        for t in newTraj.durations :
            self.durations.append(t)
        for t in newTraj.listOfWeights :
            self.listOfWeights.append(t)
    
    ### This fonction computes the difference between a recorded timedTrajectory (a list of [time, position]) and a approximated trajectory (a list of polynoms) 
    def delta(self, recordedTimedTraj):
        if self.motor.id != recordedTimedTraj.motor.id :
            raise ValueError("Trying to use delta function on 2 timedTrajectories that don't belong to the same motor.")
        if len(self.listOfTrajCoefs) < 1 :
            raise ValueError("Trying to use delta function but the calling timedTrajetory has no list of polynoms")
        result = TimedTrajectory(self.motor)
        durationIndex = 0
        shift = 0
        tempYPoly = []
        for p in recordedTimedTraj.listOfTimedPositions :
            t = p[0] - shift
            y = p[1]
            if (t*10000 < self.durations[durationIndex]) :
                #we're still in the same portion of time
                evalApprox = evalPoly(t, self.listOfTrajCoefs[durationIndex])*360/4096.0
                tempYPoly.append(evalApprox)
                delta = evalApprox - y
#                 if (self.motor.id == 4) :
#                     print "durationIndex = ", durationIndex, ", t = ", t, "evalApprox = ", evalApprox, "y = ", y, "delta = ", delta
                result.listOfTimedPositions.append([t + shift, delta])
            else :
                #We need to change the portion of time and shift t
                if (durationIndex >= (len(self.durations) - 1)) :
                    #The recorded traj is just longer than the approximated traj
#                     if (self.motor.id == 4) :
#                         print "Temp plot (exit cosrecorded traj is longer) ..."
#                         t = []
#                         for p in result.listOfTimedPositions :
#                             t.append(p[0])
#                         plt.plot(t, tempYPoly, "*")
#                         plt.show()
                    return result
                else :
                    shift = shift + self.durations[durationIndex]/10000.0
                    durationIndex = durationIndex + 1
                    t = p[0] - shift
                    evalApprox = evalPoly(t, self.listOfTrajCoefs[durationIndex])*360/4096.0
                    tempYPoly.append(evalApprox)
                    delta = evalApprox - y
                    result.listOfTimedPositions.append([t + shift, delta])
                    
            
#         if (self.motor.id == 4) :
#             print "Temp plot ..."
#             t = []
#             for p in result.listOfTimedPositions :
#                 t.append(p[0])
#             plt.plot(t, tempYPoly, "*")
#             plt.show()
         
        return result
    
    def plot(self, decorator="."):
        t = []
        y = []
        for p in self.listOfTimedPositions :
            t.append(p[0])
            y.append(p[1])
        plt.plot(t, y, decorator)
        plt.show()

    def getPlottable(self):
        t = []
        y = []
        for p in self.listOfTimedPositions :
            t.append(p[0])
            y.append(p[1])
        return t, y

    def getPlottableFromPolyCoefs(self, tVector=[]):
        durationIndex = 0
        previousT = 0
        shift = 0
        y = []
#         print "In getPlottableFromPolyCoefs, durations = ", self.durations
        if (len(tVector) < 1) :
            #Creating an arbitrary time vector :
            tMax = 0
            for duration in self.durations :
                tMax = tMax + duration
            tVector = numpy.arange(0, tMax, 0.02)
        
        for time in tVector :
            t = time - shift
            if (t*10000 < self.durations[durationIndex]) :
                #we're still in the same portion of time
                evalApprox = evalPoly(t, self.listOfTrajCoefs[durationIndex])*360/4096.0
                y.append(evalApprox)
            else :
                #We need to change the portion of time and shift t
                if (durationIndex >= (len(self.durations) - 1)) :
                    #The recorded traj is just longer than the approximated traj
                    #Trimming the tVector to make sure it has the same size than y
                    tVector = tVector[:len(y)]
                    return tVector, y
                else :
                    shift = shift + self.durations[durationIndex]/10000.0
                    durationIndex = durationIndex + 1
                    #Updating t
                    t = time - shift
                    evalApprox = evalPoly(t, self.listOfTrajCoefs[durationIndex])*360/4096.0
                    y.append(evalApprox)
                    
        return t, y
    
    ### vipCoeff is used to weight more the first and last vipLenght points. This will take effect in the polyFit functions, which minimizes the square error.
    def approximateTraj(self, nbPointsPerPoly=25, polyDegree=4, vipCoeff=5, vipLength=5, durations=[]):
        t = []
        y = []
        w = []
        tTemp = []
        yTemp = []
        wTemp = []
        t0 = 0 # self.listOfTimedPositions[0][0] #That hack tho (usefull when hacking off a portion of listOfTimedPositions)
        print "t0 = ", t0
        previousT = 0
        i = 0
        self.listOfTrajCoefs = []
        self.durations = []
        

        #If durations are specified, the other arguments (except polyDegree) are ignored.
        #This is useful when we're trying to apply approxTraj on a timedTrajectory A but want to use the result on a timedTrajectory B,
        #passing the durations list from timedTrajectory B forces the self.durations to match those of the timeTrajectory B
        if (len(durations) != 0) :
            if (durations[-1] == 0) :
                durations = durations[:-1]
            self.durations = durations
            forcedDuration = True
            print "In approximateTra, nb durations forced = ", len(durations)
        else :
            forcedDuration = False
            
        if forcedDuration == False :
            for p in self.listOfTimedPositions :
                tTemp.append(p[0] - t0)
                yTemp.append(p[1]*4096/360.0)
                if ( (i < vipLength) or (i >= (nbPointsPerPoly - vipLength)) ):
                    wTemp.append(vipCoeff)
                else :
                    wTemp.append(1)
                
                i = i + 1
                previousT = p[0]
                if (i >= nbPointsPerPoly) :
                    #End of a portion of time
                    self.durations.append(int((p[0] - t0)*10000))
                    t.append(tTemp)
                    y.append(yTemp)
                    w.append(wTemp)
                    tTemp = []
                    yTemp = []
                    wTemp = []
                    t0 = p[0]
                    i = 0
                    
            if (i != 0) :
                #The last portion of time is missing, adding it
                t.append(tTemp)
                y.append(yTemp)
                w.append(wTemp)
                #Ugly stuff
                self.durations.append(int((self.listOfTimedPositions[-1][0] - previousT)*10000))
        else :
            durationIndex = 0
            for p in self.listOfTimedPositions :
                if ((p[0] - t0)*10000 <= durations[durationIndex]) :
                    #Still in the same period of time
                    tTemp.append(p[0] - t0)
                    yTemp.append(p[1]*4096/360.0)
                    wTemp.append(1)
                else :
                    #End of a portion of time
                    t.append(tTemp)
                    y.append(yTemp)
                    w.append(wTemp)
                    tTemp = []
                    yTemp = []
                    wTemp = []
                    t0 = t0 + durations[durationIndex]/10000.0
                    tTemp.append(p[0] - t0)
                    yTemp.append(p[1]*4096/360.0)
                    wTemp.append(1)
                    durationIndex = durationIndex + 1
                    if (durationIndex >= len(durations)) :
                        break
            if (len(tTemp) != 0) :
                #The last portion has not been added yet
                print "These 2 values should be close enough, or we're in trouble : ", durations[durationIndex], " and ", tTemp[-1]*10000
                t.append(tTemp)
                y.append(yTemp)
                w.append(wTemp)
        
        coefs = []
#         sum = 0
        for index in range(len(t)) :
            tempCoefs, extra, a, b, c = numpy.polyfit(t[index], y[index], 4,full=True, w=w[index])
#             if (self.motor.id == 4 and extra[0] > 100) :
#                 print "___________________________________________________________________"
#                 print "Residual = ", extra[0]
#                 poly = numpy.poly1d(tempCoefs)
#                 plt.plot(t[index], y[index], "--", t[index], poly(t[index]), "+")
#                 plt.show()

#             print "Residuals = ", extra[0], " for t = [", t[index][0] + sum, ", ", t[index][-1] + sum, "]"
#             sum = sum + t[index][-1]
            coefs.append(tempCoefs)
        
        for index in range(len(coefs)) :
            #Reversing the order in order to have : a0 + a1*t + etc
            self.listOfTrajCoefs.append([coefs[index][4], coefs[index][3], coefs[index][2], coefs[index][1], coefs[index][0]])
#         plt.plot(t[0], y[0], 
#                  t[1] + t[0][len(t[0]) - 1], y[1], 
#                  t[2] + t[1][len(t[1]) - 1], y[2], 
#                  t[3] + t[2][len(t[2]) - 1], y[3], 
#                  t[4] + t[3][len(t[3]) - 1], y[4], 
#                  t[5] + t[4][len(t[4]) - 1], y[5])
#         plt.show()
        
    #Evaluates the list of polynoms to the given durations and adds the values to the listOfTimedPositions
    def addCorrectionPolynom(self, polyCoefs, durations) :
        if (len(polyCoefs) != len(durations)) :
            raise ValueError("In addCorrectionPolynom, polyCoefs ({0}) and durations ({1}) don't have the same size".format(len(polyCoefs, len(durations))))
        durationIndex = 0
        i = 0
        shift = 0
        for i in range(len(self.listOfTimedPositions)) :
            t = self.listOfTimedPositions[i][0] - shift
            if (t*10000 <= durations[durationIndex]) :
                #Still in the same period of time
                value = evalPoly(t, polyCoefs[durationIndex])*360/4096.0
                print "t = ", t, ", poly = ", value
                self.listOfTimedPositions[i][1] = self.listOfTimedPositions[i][1] + value
            else :
                #End of a portion of time
                shift = shift + durations[durationIndex]/10000.0
                durationIndex = durationIndex + 1
                if (durationIndex >= len(durations) or durations[durationIndex] == 0) :
                    break
                value = evalPoly(t, polyCoefs[durationIndex])*360/4096.0
                print "t = ", t, ", poly = ", value
                self.listOfTimedPositions[i][1] = self.listOfTimedPositions[i][1] + value
        
    def getDerivate(self):
        t = []
        y = []
        result = []
        for p in self.listOfTimedPositions :
            t.append(p[0])
            y.append(p[1])
#         derivate = map(truediv, y, t)
        derivate = numpy.diff(y)/numpy.diff(t)
        for i in range(len(derivate)) :
            result.append([t[i],derivate[i]])
   
        return result
    def smoothTrajectory(self, size):
        y = []
        for p in self.listOfTimedPositions :
            y.append(p[1])
        y = smooth(y, size)
        for i in range(len(self.listOfTimedPositions)) :
            self.listOfTimedPositions[i][1] = y[i]
            
    def __repr__(self):
        result = "TimedTrajectory for motor " + str(self.motor.id) + " :\n"
        for p in self.listOfTimedPositions :
            result = result + str(p[0]) + " " + str(p[1]) + "\n"
        return result
    
    def addBSTraj(self):
        #Attention 
        BSCoefs = [0.0, 0.0, 0.0, 0.0, 0.0]
        BSCoefs[0] = evalPoly(self.durations[-1]/10000.0, self.listOfTrajCoefs[-1])
        self.listOfTrajCoefs.append(BSCoefs)
        self.durations.append(5000)
    
def evalPoly(x, coefs):
    result = coefs[0] + coefs[1]*x + coefs[2]*x**2 + coefs[3]*x**3 + coefs[4]*x**4 
    return result
    
def smooth(y, size):
    box = numpy.ones(size)/size
    ySmooth = numpy.convolve(y, box, mode='same')
    return ySmooth   
    
    