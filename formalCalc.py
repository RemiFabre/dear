from __future__ import division

import math
import numexpr
import numpy
import scipy
import scipy.interpolate

import matplotlib.pyplot as plt
import scipy.optimize as opt
import trajectory3D
from subprocess import Popen, PIPE

globFinalPos = 0
globMaxAcceleration = 0
globInitSpeed = 0
globFinalSpeed = 0

def minJerk(initial, final, duration, init_vel=0.0,  init_acc=0.0,  final_vel=0.0, final_acc=0.0):
    """ Simple function computing the minimum jerk coefs and returning a lambda function.
    The general equation is: x(t) = a0 + a1 * t + a2 * t**2 + a3 * t**3 + a4 * t**4 + a5 * t**5
    Considering xi, xf, dotxi, dotxf, dotdotxi and dotdotxf being respectively the initial position, final position
    initial velocity, final velocity, initial acceleration and final acceleration.
    We have to solve the system:

    a3*d**3+a4*d**4+a5*d**5=xf-a0-a1*d-a2*d**2
    3*a3*d**2+4*a4*d**3+5*a5*d**4=dotxf-a1-2*a2*d
    6*a3*d+12*a4*d**2+20*a5*d**3=dotdotxf-2*a2

    """

    a0 = initial
    a1 = init_vel
    a2 = init_acc / 2.0

    d5 = duration ** 5
    d4 = duration ** 4
    d3 = duration ** 3
    d2 = duration ** 2
    A = numpy.array(
        [[d3, d4, d5], [3 * d2, 4 * d3, 5 * d4], [6 * duration, 12 * d2, 20 * d3]])
    B = numpy.array([final - a0 - (a1 * duration) - (a2 * d2),
                  final_vel - a1 - (2 * a2 * duration), final_acc - (2 * a2)])

    #Solves AX + B = 0
    X = numpy.linalg.solve(A, B)

    print a0, " ", a1, " ", a2, " ", X[0], " ", X[1], " ", X[2]
    return lambda x: a0 + a1 * x + a2 * x ** 2 + X[0] * x ** 3 + X[1] * x ** 4 + X[2] * x ** 5


# Simple primitive with a constant speed (infinite acceleration at the begin and end though)
def constantSpeed(distance, totalTime) :
    return lambda x : (distance / totalTime) * x

# Brutally gives the destination point
def brutal(distance) :
    return lambda x : distance

def sign(x) :
    return math.copysign(1, x)


# finalPos is suposed to be positive
def maxAccelMano(finalPos, maxAcceleration, maxSpeed, initSpeed=0, finalSpeed=None, printMode=False):
    a2 = maxAcceleration/2
    a1 = initSpeed
    a0 = 0
    # tms = Time to get to max speed with a positive bang. ts = Time to start the negative bang. T = total trajectory time
    tms = (maxSpeed - initSpeed)/(2*a2)

    function1 = lambda t : a0 + a1 * t + a2 * t * t
    functionMaxSpeed = lambda t : (t-tms) * maxSpeed + function1(tms)

    if (finalSpeed == None or finalSpeed >= maxSpeed) :
        T = (- a1 + math.sqrt(a1*2 + 4*finalPos*a2)) / (2 * a2)
        ts = T
        #A single second degree polynome + a constant function will do the trick
        b0 = a0
        b1 = a1
        b2 = a2
    else :
        #We probably need two second degree polynomes to solve this
        b2 = -a2
        #Solved with sympy (works)
        Xf = finalPos
        vf = finalSpeed
        [b1, b0, ts, T] = [(-a1*a2 + math.sqrt(2)*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)))/a2,
                           (-4*Xf*a2**2 - 3*a1**2*a2 + 2*math.sqrt(2)*a1*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)) - a2*vf**2)/(4*a2**2),
                           (-2*a1*a2 + math.sqrt(2)*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)))/(4*a2**2),
                           (-a2*(a1 + vf) + math.sqrt(2)*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)))/(2*a2**2)]

        if (T < 0 or ts < 0) :
            [b1, b0, ts, T] = [-(a1*a2 + math.sqrt(2)*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)))/a2,
                               -(4*Xf*a2**2 + 3*a1**2*a2 + 2*math.sqrt(2)*a1*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)) + a2*vf**2)/(4*a2**2),
                               -(2*a1*a2 + math.sqrt(2)*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)))/(4*a2**2),
                               -(a2*(a1 + vf) + math.sqrt(2)*math.sqrt(a2**2*(4*Xf*a2 + a1**2 + vf**2)))/(2*a2**2)]

        if (ts > tms) :
            #The solution implies a bigger speed than tolerated, hence we need to limit it
            delta_T_ts = abs((maxSpeed - finalSpeed) / (2*b2)) #Time to get from max speed to final speed
            ts = tms + (finalPos - delta_T_ts*(maxSpeed + finalSpeed)/2 - function1(tms))/maxSpeed
            b1 = maxSpeed #Speed continuity
            b0 = function1(tms) + (ts - tms)*maxSpeed #Position continuity
            T = delta_T_ts + ts
        else :
            tms = ts

    function2 = lambda t : b0 + b1 * (t - ts) + b2 * (t - ts)**2

    pwFunction = lambda t : numpy.piecewise(numpy.array([t]),
                                                [numpy.array([t]) < tms,
                                                 numpy.array([t]) >= tms and numpy.array([t]) <= ts,
                                                 numpy.array([t]) > ts],
                                                [function1, functionMaxSpeed, function2])[0]
    if (printMode ==  False) :
        return [pwFunction, T]
    else :
        # Useful if you want to compare position vs speed vs accel
        speed1 = lambda t : a1 + 2 * a2 * t
        speedMaxSpeed = lambda t : maxSpeed
        speed2 = lambda t : b1 + 2 * b2 * (t - ts)
        pwSpeed = lambda t : numpy.piecewise(numpy.array([t]),
                                                [numpy.array([t]) < tms,
                                                 numpy.array([t]) >= tms and numpy.array([t]) <= ts,
                                                 numpy.array([t]) > ts],
                                                [speed1, speedMaxSpeed, speed2])[0]
        accel1 = lambda t : 2 * a2
        accelMaxSpeed = lambda t : 0
        accel2 = lambda t : 2 * b2
        pwAccel = lambda t : numpy.piecewise(numpy.array([t]),
                                                [numpy.array([t]) < tms,
                                                 numpy.array([t]) >= tms and numpy.array([t]) <= ts,
                                                 numpy.array([t]) > ts],
                                                [accel1, accelMaxSpeed, accel2])[0]
        return [pwFunction, pwSpeed, pwAccel, tms, ts, T]

#Used for the numeric solving of the limited accel problem
def fAccel(variables):
    a2 = globMaxAcceleration * sign(globFinalPos)/2
    a1 = globInitSpeed
    a0 = 0
    b2 = -a2

    (b0, b1, ts, T) = variables

    eq1 = b1 + 2*b2*T - globFinalSpeed #Final speed
    eq2 = b0 + b1*T + b2*(T**2) - globFinalPos  # Final position
    eq3 = a0 + a1*ts + a2*(ts**2) - b0 - b1*ts - b2*(ts**2) #poly1(ts) == poly2(ts) -> position is continuous
    eq4 = a1 + 2*a2*ts - b1 - 2*b2*ts #poly1'(ts) == poly2'(ts) -> speed is continuous

    # print "b0 = " + str(b0) + ", b1 = " + str(b1)
    # print abs(eq1) + abs(eq2) + abs(eq3) + abs(eq4)
    return [eq1, eq2, eq3, eq4]

def iAmAnArrayISwear(x) :
    return [x]

def prettyFunction1(xOffset, yOffset, printIt=False) :
    m = 16
    n = 4
    pp = numpy.pi*(m+n)/2.0
    pm = numpy.pi*(m-n)/2.0
    #Y ranges : [-5 to -25]
    #X ranges = [7 to 19]
    # Workign square is X = [7, 14], Y = [-5, -19]
    xFunction = lambda t : (3.5*numpy.cos(pp*t)*numpy.cos(pm*t) + xOffset)/100.0
    yFunction = lambda t : (3.5*numpy.sin(pp*t)*numpy.cos(pm*t) + yOffset)/100.0
    t = numpy.arange(0, 0.5, 1/1000.0)
    x = map(xFunction, t)
    y = map(yFunction, t)
    
    if (printIt) :
        plt.plot(x, y)
        plt.show()
    return x, y

def prettyFunction2(printIt=False) :
    xFunction = lambda t : 0.16
    yFunction = lambda t : -0.1
    t = numpy.arange(0, 0.5, 1/1000.0)
    x = map(xFunction, t)
    y = map(yFunction, t)
    
    if (printIt) :
        plt.plot(x, y)
        plt.show()
    return x, y

def writeTrajectory(x, y, z, dt, nbPointsPerPoly, toString=False) :
    traj3D = trajectory3D.Trajectory3D()
    t = 0
    penDownDuration = 0.5
    height = 0.01
    nbIterations = int(penDownDuration/dt)
    dz = height/nbIterations
    tempZ = z - height
    #Pen down
    for i in range(nbIterations) :
        traj3D.listOfTimedPositions.append([t, x[0], y[0], tempZ])
        tempZ = tempZ + dz
        t = t + dt
    #Drawing
    for i in range(len(x)) :
        traj3D.listOfTimedPositions.append([t, x[i], y[i], z])
        t = t + dt
    
    #Pen up
    tempZ = z
    for i in range(nbIterations) :
        traj3D.listOfTimedPositions.append([t, x[0], y[0], tempZ])
        tempZ = tempZ - dz
        t = t + dt
    print "Trajectory total duration = ", t
    traj3D.approximateTraj(nbPointsPerPoly=nbPointsPerPoly, polyDegree=4, vipCoeff=4, vipLength=nbPointsPerPoly/5)
    if (toString == True) :
        return traj3D.saveToString()
    else :
        traj3D.saveToFile("prettyFunction1.rec")
        plottable = traj3D.getPlottable()
        plt.plot(plottable[0], plottable[1], "x", 
                         plottable[0], plottable[2], "--", 
                         plottable[0], plottable[3], ",")
    #                                              tPlot[0], tPlot[1], "--")
        plt.legend(['x', 'y', 'z']) #'sent torque'])
        plt.grid(True)
        plt.show()

def comTest() :
    command = "../Model/build/appTestLegTorques"
    arg = "prettyFunction1.rec"

    process = Popen([command, arg], stdout=PIPE)
    (output, err) = process.communicate()
    lines = output.split('\n')
    for l in lines :
        #Retrieving torque information
        print l
            
    exit_code = process.wait()

def main() :
#     comTest()
#     return
    x, y = prettyFunction1(printIt=True)
    writeTrajectory(x, y)
    return
#     duration = 0.5
#     amplitude = 170
#     t = numpy.arange(0, duration, duration/100)
#     y1 = map(numpy.sin, t*numpy.pi/(2*duration))
#     spline1 = scipy.interpolate.UnivariateSpline(t, y1, w=None, bbox=[None, None], k=4, s=None)
#     
#     y2 = map(numpy.sin, (t + duration)*numpy.pi/(2*duration))
#     spline2 = scipy.interpolate.UnivariateSpline(t, y2, w=None, bbox=[None, None], k=4, s=None)
#     print "Coeffs 2 : ",  amplitude * spline2.get_coeffs()
#     
#     y3 = map(numpy.sin, (t + 2*duration)*numpy.pi/(2*duration))
#     spline3 = scipy.interpolate.UnivariateSpline(t, y3, w=None, bbox=[None, None], k=4, s=None)
#     
#     y4 = map(numpy.sin, (t + 3*duration)*numpy.pi/(2*duration))
#     spline4 = scipy.interpolate.UnivariateSpline(t, y4, w=None, bbox=[None, None], k=4, s=None)
#     
#     plt.plot(t,  amplitude * spline1(t), t + duration,  amplitude * spline2(t), t + 2*duration,  amplitude * spline3(t), t + 3*duration,  amplitude * spline4(t))
#     plt.show()
    
    polys = []
    duration = 2
    amplitude = 170
    T = numpy.arange(0, duration, duration/100)
    t = numpy.arange(0, duration/4.0, duration/25)
    
    myFunction = lambda x : amplitude*numpy.sin(x)
    timeFunction = lambda x : 2*x*numpy.pi/(duration)
    timeFunction1 = lambda x : 2*x*numpy.pi/(duration)
    timeFunction2 = lambda x : 2*(x + duration/4)*numpy.pi/(duration)
    timeFunction3 = lambda x : 2*(x + duration/2)*numpy.pi/(duration)
    timeFunction4 = lambda x : 2*(x + 3*duration/4)*numpy.pi/(duration)
    
    y = map(myFunction, timeFunction(T))
    y1 = map(myFunction, timeFunction1(t))
    y2 = map(myFunction, timeFunction2(t))
    y3 = map(myFunction, timeFunction3(t))
    y4 = map(myFunction, timeFunction4(t))
    
    coefs = numpy.polyfit(T, y, 4)
    coefs1 = numpy.polyfit(t, y1, 4)
    coefs2 = numpy.polyfit(t, y2, 4)
    coefs3 = numpy.polyfit(t, y3, 4)
    coefs4 = numpy.polyfit(t, y4, 4)
    
    poly = numpy.poly1d(coefs) #poly1(t) works
    poly1 = numpy.poly1d(coefs1) #poly1(t) works
    poly2 = numpy.poly1d(coefs2) #poly1(t) works
    poly3 = numpy.poly1d(coefs3) #poly1(t) works
    poly4 = numpy.poly1d(coefs4) #poly1(t) works
    
    #Reversing the order cos these guys are crazy
    polys.append([coefs1[4], coefs1[3], coefs1[2], coefs1[1], coefs1[0]])
    polys.append([coefs2[4], coefs2[3], coefs2[2], coefs2[1], coefs2[0]])
    polys.append([coefs3[4], coefs3[3], coefs3[2], coefs3[1], coefs3[0]])
    polys.append([coefs4[4], coefs4[3], coefs4[2], coefs4[1], coefs4[0]])
    
    print polys
    
    plt.plot(T, myFunction(timeFunction(T)), 
             t,  poly1(t), '*', 
             t + duration/4,  poly2(t), '*', 
             t + duration/2,  poly3(t), '*', 
             t + 3*duration/4,  poly4(t), '*', 
             T, poly(T))

    plt.show()
    
    return

    #scipy.interpolate.PiecewisePolynomial(t, y, orders=None, direction=None, axis=0)
    
    # We just call opt passing the function and an initial value
    # solution = opt.fsolve(f,(0.1,0))
    # print "The solution is",solution

    jerk = minJerk(0, 2048, 1)
    vector = numpy.linspace(0, 1, 100)
    for x in vector :
        print x, " ", jerk(x)

    # solution = maxAccelMano(100, 4, 10, 0, 0, printMode=True)

    # tms = solution[3]
    # ts = solution[4]
    # T = solution[5]

    # # print "tms = ", tms
    # # print "ts = ", ts
    # # print "T = ", T

    # vector = numpy.linspace(0, T, 100)
    # for x in vector :
    #     print x, " ", solution[0](x), " ", solution[1](x), " ", solution[2](x)


    # T = solution[3]
    # ts = solution[2]
    # print "T = ", T
    # print "ts = ", ts

    # vector = numpy.linspace(0, T, 100)
    # for x in vector :
    #     if (x <= ts) :
    #         print x, solution[0](x), solution[4](x), solution[6](x)
    #     else :
    #         print x, solution[1](x), solution[5](x), solution[7](x)

if __name__ == '__main__':
    main()
    

# def test(x) :
#     x = numpy.linspace(x, x, 1)
#

# class PiecewiseFunction():
#     def __init__(self, separationValues, functions) :
#         self.conditions = conditions
# 	self.separationValues = separationValues

#     def eval(self, x) :
#         x = numpy.linspace(x, x, 1)

#         conditions = []
#         for v in separationValues :
#             conditions.append(x < v)
#         lastSeparation = separationValues[len(separationValues) - 1]
#         conditions.append(x >= lastSeparation)

#         return numpy.piecewise(x, conditions, functions)[0]

