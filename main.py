"""
To do :
- Spline the trajectories
- Investigate why orders often fail when the servo is moving (is it the firmware or the hardware?)
- Update servo firmware as written in the specs
- Update servo firmaware in order to have CW and CCW angle limits
- Calibrate each servo inner model and do crazy stuff with it
"""

from contextlib import closing
import matplotlib.pyplot as plt
import json
import logging
import logging.config
import math
import pypot.dynamixel
import pypot.robot
import sys
import time

from display import createDisplay
import display
from utils import *
import numpy

class RobotManager(object):
    def __init__(self) :
        self.dxl_io = None

    def main(self):
        LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,

        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
            'simple': {
                'format': '%(levelname)s %(asctime)s %(message)s'
            },
        },

        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': 'pypot.log',
                'mode' : 'w',
                'formatter' : 'simple'
            },
        },

        'loggers': {
            'pypot': {
                'handlers': ['file', ],
                'level': 'INFO',#'DEBUG',
            },
        },
    }

        logging.config.dictConfig(LOGGING)

#         ports = pypot.dynamixel.get_available_ports()
#         if not ports:
#             raise IOError('no port found!')
#         self.dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)
#         self.calibrateTorque(self.dxl_io)
# #         self.produceTorque(self.dxl_io)
# #         self.printTorques(self.dxl_io)  
# #        self.singleMotorTest(self.dxl_io)
#         return
         
                #The robot interface works now
        with closing(pypot.robot.from_json('robotConfigWithAngles.json')) as robot:
            print "Robot loaded with success !"
            
            print "Setting PID values"
            i = 0
            
            while False :
                for m in robot.motors :
                    m.compliant = True
                    print m
                time.sleep(0.2)
            
            time.sleep(0.2)       
            
            for m in robot.motors:
                if (i == 0 or i == 1) :
                    i = i + 1
                    m.pid = [4.0, 0.0, 0.0]
                    continue
                # default values are : [2, 0.0, 0.0]
                m.pid = [6.0, 16.0, 0.0]
                print m.pid
                i = i + 1
                
            for m in robot.motors :
                m.compliant = False
#             while True :
#                 for m in robot.motors :
#                         print m
#                 time.sleep(0.02)

            while False :
                for m in robot.motors :
                    print m
                temp = raw_input("Motor 1 goal position : ")
                if (temp != "") :
                    robot.motor_1.goal_position = float(temp)
                temp = raw_input("Motor 2 goal position : ")
                if (temp != "") :
                    robot.motor_2.goal_position = float(temp)
                temp = raw_input("Motor 3 goal position : ")
                if (temp != "") :
                    robot.motor_3.goal_position = float(temp)
                temp = raw_input("Motor 4 goal position : ")
                if (temp != "") :
                    robot.motor_4.goal_position = float(temp)
                temp = raw_input("Motor 5 goal position : ")
                if (temp != "") :
                    robot.motor_5.goal_position = float(temp)
                temp = raw_input("Motor 6 goal position : ")
                if (temp != "") :
                    robot.motor_6.goal_position = float(temp)
            
            params = Parameters(robot, 0, 50)

            createDisplay(params)
             
        return
        
        ports = pypot.dynamixel.get_available_ports()
        if not ports:
            raise IOError('no port found!')
        print 'ports found', ports
        print 'connecting on the first available port:', ports[0]
        self.dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)
#         self.dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=57600)

        print "Positions = ", self.dxl_io.get_present_position([1,2,3,4])

        goalPositions = [(1,-22), (2,180), (3,180), (4, -158)]
#         self.dxl_io.enable_torque([1,2,3,4])
        self.dxl_io.enable_torque([1, 2])    
        print "Going to init position : ", goalPositions
        self.smoothlyGoToPosition(self.dxl_io, goalPositions, 4, 50)
        
        return
        t0 = time.time()
#         while True :
#             errorCounter = 0
#             t = time.time() - t0
#             deltaPos = 35*math.sin(8*t)
#             # print t
#             try :
#                 self.dxl_io.set_goal_position({1:int(initPos[0]+deltaPos), 2:int(initPos[1]+deltaPos), 3:int(initPos[2]+deltaPos), 4:int(initPos[3]+deltaPos)})
# #                 self.dxl_io.set_goal_position({1:int(initPos[0]+deltaPos), 2:int(initPos[1]+deltaPos)})
# #                 self.dxl_io.set_goal_position({1:int(initPos[0]+deltaPos)})
#                 print 'time = ', t
# 
# 
#             except :
#                 errorCounter = errorCounter + 1
#                 pass
#             # self.dxl_io.set_goal_position({1:int(initPos[0]+deltaPos)})
#             # self.dxl_io.set_goal_position({1:159, 2:90, 3:0, 4:28})
#             # positions = self.dxl_io.get_present_position([1,2,3,4])
#             # print positions
#             
#             print "Nb errors = ", errorCounter
#             time.sleep(0.005)
# 
#         return

        self.singleMotorTest(self.dxl_io)
        return


    def singleMotorTest(self, dxlIo) :
        print "Test with PID only:"
        self.dxl_io.set_mode_dynaban({1:0})
        time.sleep(0.1)
        self.dxl_io.enable_torque({1:1})
        time.sleep(0.1)
        dxlIo.set_goal_position({1:90})
        time.sleep(1)
        dxlIo.set_pid_gain({1:[1,0,0]})
        time.sleep(0.1)

        print "Setting traj1 :"
        
#         self.setTraj1(1, 60000, [2000.0, -4223.0, 4355.8, -1337.3, 122.21])
#         self.setTraj1(1, 60000, [0.0, 2000.0, 0.0, 0.0, 0.0])

#         self.setTorque1(1, 60000, [2000.0, -4223.0, 4355.8, -1337.3, 122.21])
#         self.setTraj2(1, 60000, [0.0, 0.0, -300.0, 0.0, 0.0])
#         self.setTorque2(1, 60000, [0.0, 0.0, 0.0, 0.0, 0.0])

        
#         Temp :
#         self.dxl_io.set_mode_dynaban({1:5})
#         time.sleep(0.2)

        # 4 quarter of Sinus :
# [[0.0029282637700648157, 532.49287882689293, 29.07849099701108, -1058.1470413492355, 459.3664329672057], 
# [169.9973127875532, 1.2118904739507608, -859.49525560910968, 109.93882674890278, 489.17556618589202], 
# [-0.0029282637700933502, -532.49287882689202, -29.078490997017791, 1058.1470413492527, -459.36643296722087], 
# [-169.99731278755326, -1.2118904739506096, 859.49525560910888, -109.93882674889758, -489.17556618590021]]

        self.setTraj1(1, 5000, [0.0 - 1024.0, 532.49287882689293, 29.07849099701108, -1058.1470413492355, 459.3664329672057])
        
        # Sinus in 1 poly :
#         self.setTraj1(1, 20000, [-29.674295847526356, 984.0598208069265, -1405.6540932494643, 438.30193405296455, 13.613605024087844])

        
        print "Setting mode and tracking :"
#         self.dxl_io.set_position_tracker_on({1:1}) # The motor wil print out its actual traj
        self.dxl_io.set_mode_dynaban({1:2}) # 1 = predictive only # 2 = PID only, # 3 = PID + predictive, # 4 = compliant kind of, # 5 = no strings attached
        print "Sleeping"
        time.sleep(0.15)
        while True :
            print "Traj2"
            self.setTraj2(1, 5000, [169.9973127875532 - 1024.0, 1.2118904739507608, -859.49525560910968, 109.93882674890278, 489.17556618589202])
            self.dxl_io.set_copy_next_buffer({1:1}) # buffer 2 will be copied into buffer 1 once the traj 1 finishes
            time.sleep(0.5)
            print "Traj3"
            self.setTraj2(1, 5000, [0.0 - 1024.0, -532.49287882689202, -29.078490997017791, 1058.1470413492527, -459.36643296722087])
            self.dxl_io.set_copy_next_buffer({1:1}) # buffer 2 will be copied into buffer 1 once the traj 1 finishes
            time.sleep(0.5)
            print "Traj4"
            self.setTraj2(1, 5000, [-169.99731278755326 - 1024.0, -1.2118904739506096, 859.49525560910888, -109.93882674889758, -489.17556618590021])
            self.dxl_io.set_copy_next_buffer({1:1}) # buffer 2 will be copied into buffer 1 once the traj 1 finishes
            time.sleep(0.5)
            
            print "Traj1"
            self.setTraj2(1, 5000, [0.0 - 1024.0, 532.49287882689293, 29.07849099701108, -1058.1470413492355, 459.3664329672057])
            self.dxl_io.set_copy_next_buffer({1:1}) # buffer 2 will be copied into buffer 1 once the traj 1 finishes
            time.sleep(0.5)
            
            
        return
        while True :
            position = dxlIo.get_present_position([1])
            time.sleep(0.001)
            print "Setting traj2 and copy next buffer mode"
            self.setTraj2(1, 60000, [position, -4223.0, 4355.8, -1337.3, 122.21])
            print "traj 2 sent"
            for i in range(3) :
                time.sleep(0.001)
                print "spamming copy next buffer"
                self.dxl_io.set_copy_next_buffer({1:1}) # buffer 2 will be copied into buffer 1 once the traj 1 finishes
            time.sleep(6)

        print "End"
        return
        self.dxl_io.set_mode_dynaban({1:0})
        dxlIo.set_goal_position({1:0})
        time.sleep(1)

        print "Compliant mode (needs some qualibration though) :"
        self.dxl_io.set_mode({1:3})
        return

    def printTorques(self, dxlIo) :
        print "Going to init position ..."
        graphSize = 100
        setBack = 30
#         dxlIo.set_angle_limit({1:[0,0]})
#         dxlIo.set_angle_limit({2:[0,0]})
#         dxlIo.set_angle_limit({3:[0,0]})
#         dxlIo.set_pid_gain({1:[2.0, 5.0, 0.0]})
        dxlIo.set_pid_gain({1:[2.0, 0.0, 0.0]})
        dxlIo.enable_torque([1])
        dxlIo.enable_torque([2])
        dxlIo.enable_torque([3])
        dxlIo.set_goal_position({1:180})
        dxlIo.set_goal_position({2:0})
        dxlIo.set_goal_position({3:0})
        print "Printing torques ..."
        l = 0.195
        #Torque = m*g*l
        g = 9.81
        plt.ion()
        #Set up plot
        figure, ax = plt.subplots(2)
        x = []
        y = []
        y2 = []
        lines,  = ax[0].plot(x, y, '--')
        lines2, = ax[1].plot(x, y, '--')
        #Autoscale on unknown axis and known lims on the other
        ax[0].set_autoscaley_on(True)
        ax[0].set_xlim(0, 30)
        ax[0].set_autoscalex_on(True)
        ax[1].set_autoscaley_on(True)
        ax[1].set_xlim(0, 30)
        ax[1].set_autoscalex_on(True)
        #Other stuff
        ax[0].grid()
        ax[1].grid()
        t0 = time.time()
        while True :
            raw =  dxlIo.get_outputTorqueWithoutFriction([1])[0]
            torqueWithoutFriction = raw*2*numpy.pi/4096.0
            torque = dxlIo.get_outputTorque([1])[0]*2*numpy.pi/4096.0
            torqueCorrected = torque + 55.7*2*numpy.pi/4096.0
            weight = 1000*torque/(g*l)
            weightWithoutFriction = 1000*torqueWithoutFriction/(g*l)
            weightCorrected = 1000*(torqueCorrected)/(g*l)
            print "Position = ", dxlIo.get_present_position([1])
            print "weight = ", weight, " g"
            print "weightWithoutFriction = ", weightWithoutFriction, " g"
            print "weightCorrected = ", weightCorrected, " g"
            t = time.time() - t0
            x.append(t)
            y2.append( raw)#(torqueWithoutFriction)
            y.append(torque)
            if (len(x) > graphSize) :
                x = x[setBack:graphSize]
                y = y[setBack:graphSize]
                y2 = y2[setBack:graphSize]
            #Update data (with the new _and_ the old points)
            lines.set_xdata(x)
            lines.set_ydata(y)
            lines2.set_xdata(x)
            lines2.set_ydata(y2)
            #Need both of these in order to rescale
            ax[0].relim()
            ax[0].autoscale_view()
            ax[1].relim()
            ax[1].autoscale_view()
            #We need to draw *and* flush
            figure.canvas.draw()
            figure.canvas.flush_events()
            time.sleep(0.05)

    def produceTorque(self, dxlIo) :
        dxlIo.enable_torque([1])
        dxlIo.enable_torque([2])
        dxlIo.enable_torque([3])
        dxlIo.set_goal_position({1:180})
        dxlIo.set_goal_position({2:0})
        dxlIo.set_goal_position({3:0})
        time.sleep(2)
        l = 0.195
        #Torque = m*g*l
        g = 9.81
        m = 0.05
        torque = m*g*l*4096/numpy.pi
        
        print "Setting torque ..."
        self.setTraj1(1, 20000, [0.0, 0.0, 0.0, 0.0, 0.0])
        self.setTorque1(1, 20000, [torque, 0.0, 0.0, 0.0, 0.0])
        self.dxl_io.set_mode_dynaban({1:1})

        while True :
            print "Sleeping"
            time.sleep(2.5)
            
            print "Done. Compliant mode on"
            dxlIo.enable_torque({1:0})
            m = float(raw_input("m ?\n"))
            print "m = ", m, " kg"
            torque = m*g*l*4096/numpy.pi
            print "torque = ", torque, " N.m"
            dxlIo.enable_torque({1:1})
            print "Setting torque ..."
            self.setTraj1(1, 20000, [0.0, 0.0, 0.0, 0.0, 0.0])
            self.setTorque1(1, 20000, [torque, 0.0, 0.0, 0.0, 0.0])
            self.dxl_io.set_mode_dynaban({1:1})
            
    def calibrateTorque(self, dxlIo) :
        dxlIo.set_pid_gain({1:[4.0, 4.0, 0.0]})
        dxlIo.set_pid_gain({2:[4.0, 4.0, 0.0]})
        dxlIo.set_pid_gain({3:[4.0, 4.0, 0.0]})
        dxlIo.enable_torque([1])
        dxlIo.enable_torque([2])
        dxlIo.enable_torque([3])
        dxlIo.set_angle_limit({1:[0.0,0.0]})
        raw_input("Go?")
#         self.smoothlyGoToPosition(dxlIo, [[1,180.0], [2,0.0], [3,0.0]], timeToDestination=2, freq=50)
        time.sleep(1)
        failure = 176.0
        success = -176.0
        maxM = 0.5
        low = 0.0
        high = maxM
        period = 3000 #300 ms 
        l = 0.195 #0.185 is more accurate
        #Torque = m*g*l
        g = 9.81
        m = 0.025
        maxTime = 2.5
        precision = 0.002
        done = False

        while True :
            if (abs(high - low) < precision) :
                break
            self.smoothlyGoToPosition(dxlIo, [[1,180.0], [2,0.0], [3,0.0]], timeToDestination=1, freq=50)
            time.sleep(1)
            print "m = ", m, " kg. High = ", high, ", low = ", low
            torque = m*g*l*4096/(2*numpy.pi)
            print "torque to be sent = ", torque, " N.m"
#             raw_input("Go ?")
            self.setTraj1(1, period, [0.0, 0.0, 0.0, 0.0, 0.0])
            self.setTorque1(1, period, [torque, 0.0, 0.0, 0.0, 0.0])
            self.dxl_io.set_mode_dynaban({1:1})
            t0 = time.time()
            t02 = t0
            t = 0
            t2 = 0
            torquesWithoutFriction = []
            torques = []
            
            while t < maxTime:
                angle = float(dxlIo.get_present_position([1])[0])
#                 print "angle = ", angle
                if (angle > 0 and angle < failure) :
                    print "=======> m = ", m, " is too weak"
                    #Not enough torque, we'll have to increase our m
                    low = m
                    m = (high + low)/2.0
                    break
                elif (angle < 0 and angle > success) :
                    print "=======> m = ", m, " is too strong"
                    #We've succeeded, we'll try with a lower m
                    high = m
                    m = (high + low)/2.0
                    break
                else :
                    #We'll make sure we keep moving
                    if (t2 > period/(2*10000.0)) :
                        self.setTraj2(1, period, [0.0, 0.0, 0.0, 0.0, 0.0])
                        self.setTorque2(1, period, [torque, 0.0, 0.0, 0.0, 0.0])
                        self.dxl_io.set_copy_next_buffer({1:1})
                        t02 = time.time()
                time.sleep(0.1)
                torquesWithoutFriction.append(dxlIo.get_outputTorqueWithoutFriction([1])[0])
                torques.append(dxlIo.get_outputTorque([1])[0])
                t = time.time() - t0
                t2 = time.time() - t02
            if (t >= maxTime) :
                print "Didn't move enough. m = ", m, " considered too weak"
                low = m
                m = (high + low)/2.0
            print "torques = ", torques
            print "torquesWithoutFriction", torquesWithoutFriction
        
        m = m * 1.471 * 1000 - 103.8
        precision = precision * 1.471 * 1000
        print "Final m = ", m, "g with precision = ", precision, " g"
        self.smoothlyGoToPosition(dxlIo, [[1,180.0], [2,0.0], [3,0.0]], timeToDestination=1, freq=50)

    def setTraj1(self, id, duration,coeffs) :
        errorCounter = 0
        delay = 0.001
        while True :
            try:
                self.dxl_io.set_traj1_size({id:5})
                time.sleep(delay)
                self.dxl_io.set_duration1({id:duration})
                time.sleep(delay)
                self.dxl_io.set_a0_traj1({id : coeffs[0]})
                time.sleep(delay)
                self.dxl_io.set_a1_traj1({id : coeffs[1]})
                time.sleep(delay)
                self.dxl_io.set_a2_traj1({id : coeffs[2]})
                time.sleep(delay)
                self.dxl_io.set_a3_traj1({id : coeffs[3]})
                time.sleep(delay)
                self.dxl_io.set_a4_traj1({id : coeffs[4]})
                time.sleep(delay)
                break
            except:
                errorCounter = errorCounter + 1
                # print "Nope :/"
                break
#         print "Nb errors : ", errorCounter

    def setTorque1(self, id, duration,coeffs) :
        errorCounter = 0
        delay = 0.001
        while True :
            try:
                self.dxl_io.set_torque1_size({id:5})
                time.sleep(delay)
                self.dxl_io.set_duration1({id:duration})
                time.sleep(delay)
                self.dxl_io.set_a0_torque1({id : coeffs[0]})
                time.sleep(delay)
                self.dxl_io.set_a1_torque1({id : coeffs[1]})
                time.sleep(delay)
                self.dxl_io.set_a2_torque1({id : coeffs[2]})
                time.sleep(delay)
                self.dxl_io.set_a3_torque1({id : coeffs[3]})
                time.sleep(delay)
                self.dxl_io.set_a4_torque1({id : coeffs[4]})
                time.sleep(delay)
                break
            except:
                errorCounter = errorCounter + 1
                # print "Nope :/"
                pass
#         print "Nb errors : ", errorCounter


    def setTraj2(self, id, duration, coeffs) :
        errorCounter = 0
        delay = 0.001

        while True :
            try:
                self.dxl_io.set_traj2_size({id:5})
                time.sleep(delay)
                self.dxl_io.set_duration2({id:duration})
                time.sleep(delay)
                self.dxl_io.set_a0_traj2({id : coeffs[0]})
                time.sleep(delay)
                self.dxl_io.set_a1_traj2({id : coeffs[1]})
                time.sleep(delay)
                self.dxl_io.set_a2_traj2({id : coeffs[2]})
                time.sleep(delay)
                self.dxl_io.set_a3_traj2({id : coeffs[3]})
                time.sleep(delay)
                self.dxl_io.set_a4_traj2({id : coeffs[4]})
                time.sleep(delay)
                break
            except:
                errorCounter = errorCounter + 1
#                 print "nb errors = ", errorCounter
                break
#         print "Nb errors : ", errorCounter

    def setTorque2(self, id, duration, coeffs) :
        errorCounter = 0
        delay = 0.001
        while True :
            try:
                self.dxl_io.set_torque2_size({id:5})
                time.sleep(delay)
                self.dxl_io.set_duration2({id:duration})
                time.sleep(delay)
                self.dxl_io.set_a0_torque2({id : coeffs[0]})
                time.sleep(delay)
                self.dxl_io.set_a1_torque2({id : coeffs[1]})
                time.sleep(delay)
                self.dxl_io.set_a2_torque2({id : coeffs[2]})
                time.sleep(delay)
                self.dxl_io.set_a3_torque2({id : coeffs[3]})
                time.sleep(delay)
                self.dxl_io.set_a4_torque2({id : coeffs[4]})
                time.sleep(delay)
                break
            except:
                errorCounter = errorCounter + 1
                # print "Nope :/"
                pass
#         print "Nb errors : ", errorCounter
        
    def smoothlyGoToPosition(self, dxlIo, goalPositions, timeToDestination=4, freq=50) :     
        print "Going to positions : ", goalPositions
        ids = []
        writingPeriod = 1.0/float(freq)

        for (id, position) in goalPositions :
            ids.append(id)
        
        #Getting positions at the begining    
        presentPositions = dxlIo.get_present_position(ids)
#         print "presentPositions = ", presentPositions
        
        i = 0
        deltas = []
        for (id, position) in goalPositions :
            delta = (position - presentPositions[i])%360
            if (delta > 180) :
                delta = -360 + delta
            deltas.append(delta)
            i = i + 1

#         print "Deltas = ", deltas
        elapsedTime = 0
        errorCounter = 0
        while elapsedTime < timeToDestination :
            progress = elapsedTime / timeToDestination
            outputCommands = {}
            #Building output vector with new commands
            i = 0
            for (id, position) in goalPositions :
                outputCommands[id] = int(presentPositions[i] + progress * deltas[i])
                i = i + 1
#             print "Vector = ", outputCommands
            try :
                dxlIo.set_goal_position(outputCommands)
            except :
                errorCounter = errorCounter + 1
                print 'errorCounter = ', errorCounter
            
#             dxlIo.set_goal_position({1:158, 2:91, 3:1, 4:-158})
            

            elapsedTime = elapsedTime + writingPeriod
#             print progress*100, "%"
            time.sleep(writingPeriod)
        
        return

    def autoDetectRobot(self):
        print("Robot detection ...")
        robot = pypot.dynamixel.autodetect_robot()
        print("Robot detected. Writing to conf file ...")
        config = robot.to_config()
        with open('robotConfig.json', 'wb') as f:
            json.dump(config, f)
        print("Robot conf file written...")


print("A new day dawns")
robMan = RobotManager()
robMan.main()
print("Done !")
