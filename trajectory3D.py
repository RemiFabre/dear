import numpy
import matplotlib.pyplot as plt
import os
from operator import truediv
from pyglet.window.xlib.xlib import XMaxCmapsOfScreen
import warnings

class Trajectory3D():
    def __init__(self) :
        self.listOfTimedPositions = []# A timed position respects the format : [time, x, y, z]
        self.listOfTrajCoefsX = []
        self.listOfTrajCoefsY = []
        self.listOfTrajCoefsZ = []
        self.listOfTrajCoefsAlpha = []
        self.durations = []
        self.listOfWeights = []
        
    def clear(self):
        self.listOfTimedPositions = []
        self.listOfTrajCoefsX = []
        self.listOfTrajCoefsY = []
        self.listOfTrajCoefsZ = []
        self.listOfTrajCoefsAlpha = []
        self.durations = []

    def saveToFile(self, fileAdress):
        try :
            os.remove(fileAdress)
        except :
            pass
        f = open(fileAdress,'w')
        print "Size of x = ", len(self.listOfTrajCoefsX)
        print "Size of y = ", len(self.listOfTrajCoefsY)
        print "Size of z = ", len(self.listOfTrajCoefsZ)
        print "Size of alpha = ", len(self.listOfTrajCoefsAlpha)
        line = "x"
        f.write(line + "\n")
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            f.write(line + "\n")
            line = ""
            for coef in self.listOfTrajCoefsX[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            f.write(line + "\n")
        line = "y"
        f.write(line + "\n")
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            f.write(line + "\n")
            line = ""
            for coef in self.listOfTrajCoefsY[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            f.write(line + "\n")
        line = "z"
        f.write(line + "\n")
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            f.write(line + "\n")
            line = ""
            for coef in self.listOfTrajCoefsZ[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            f.write(line + "\n")
        line = "alpha"
        f.write(line + "\n")
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            f.write(line + "\n")
            line = ""
            for coef in self.listOfTrajCoefsAlpha[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            f.write(line + "\n")
            
    def saveToString(self):
        output = ""
        print "Size of x = ", len(self.listOfTrajCoefsX)
        print "Size of y = ", len(self.listOfTrajCoefsY)
        print "Size of z = ", len(self.listOfTrajCoefsZ)
        print "Size of alpha = ", len(self.listOfTrajCoefsAlpha)
        line = "x"
        output += line + "\n"
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            output += line + "\n"
            line = ""
            for coef in self.listOfTrajCoefsX[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            output += line + "\n"
        line = "y"
        output += line + "\n"
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            output += line + "\n"
            line = ""
            for coef in self.listOfTrajCoefsY[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            output += line + "\n"
        line = "z"
        output += line + "\n"
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            output += line + "\n"
            line = ""
            for coef in self.listOfTrajCoefsZ[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            output += line + "\n"
        line = "alpha"
        output += line + "\n"
        for i in range(len(self.durations)) :
            line = str(self.durations[i])
            output += line + "\n"
            line = ""
            for coef in self.listOfTrajCoefsAlpha[i] :
                line = line + str(coef) + ", "
            #Taking out the last coma
            line = line[:-2]
            output += line + "\n"
        return output
        
    def readInfoFromLetterFile(self, path, scale=1.0) :
        f = open(path,'r')
        xMin = 0.0
        xMax = 0.0
        yMin = 0.0
        yMax = 0.0
        xAnchor = 0.0
        yAnchor = 0.0
        firstLine = True
        anchorFound = False

        
        for line in f :
            if len(line) == 0 :
                print "File read. Returning"
                return
            if line.startswith("U"):
                continue
            elif line.startswith("Anchor") :
                words = line.split(" ")
                yAnchor = scale*-1*float(words[1])/40000.0
                anchorFound = True
            else :
                words = line.split(" ")
                x = scale*float(words[0])/40000.0
                y = scale*-1*float(words[1])/40000.0
                
                if firstLine :
                    firstLine = False
                    xMin = x
                    xMax = x
                    yMin = y
                    yMax= y
                else :
                    if (x < xMin) :
                        xMin = x
                    if (x > xMax) :
                        xMax = x
                        if (anchorFound == False) :
                            yAnchor = y
                    if (y < yMin) :
                        yMin = y
                    if (y > yMax) :
                        yMax = y
        #The last position is the anchor
#         xAnchor = x
#         yAnchor = y
        
        return xAnchor, yAnchor, xMax, xMin, yMax, yMin
                
    def readFromXYFile(self, filePath, z, dt, scale=1.0, offsetX=0.0, offsetY=0.0, isLetter=False, xAnchor=0.0, yAnchor=0.0, xMoy=0.0):
        f = open(filePath,'r')
        height = 0.005
        standUpDuration = 0.150
        nbPointsForStandUp = 25  
        nbPointsTravel = 25
        standUpDt = standUpDuration/float(nbPointsForStandUp)
        print "dt = ", dt
        print "standUpDt = ", standUpDt
        t = 0.0
        x = offsetX
        y = offsetY
        pX = x
        pY = y
        zOffset = 0.0
        travelSpeed = 0.05
        previousLineWasU = False
        words = []
#         reduction = 4
        lineId = 0
        firstLine = True
        
        if (isLetter) :
            offsetX = offsetX - xAnchor
            offsetY = offsetY - yAnchor
        #Injecting some points to start with the pen going above the first position
        for line in f :
            if line.startswith("U") :
                continue
            else :
                words = line.split(" ")
                if (isLetter) :
                    x = (scale*float(words[0])/40000.0 + offsetX)*(-1) + 2*(xMoy + offsetX) #Axial symetry
                    y = scale*-1*float(words[1])/40000.0 + offsetY
                else :
                    x = scale*float(words[0])
                    y = scale*float(words[1])
                break
        
        zOffset = -height
        for i in range(nbPointsForStandUp) :
            zOffset = zOffset + height/float(nbPointsForStandUp)
            t = t + standUpDt
            self.listOfTimedPositions.append([t, x, y, z + zOffset])
        zOffset = 0.0
        
        f.close()
        f = open(filePath,'r')
        for line in f :
            if len(line) == 0 :
                print "File read. Returning"
#                 print self.listOfTimedPositions
                return
            if firstLine and line.startswith("U"):
                #Ignoring first U
                firstLine = False
                continue
            if line.startswith("U") :
                previousLineWasU = True
                #We're taking the pen off of the ground
                zOffset = 0.0
                for i in range(nbPointsForStandUp) :
                    zOffset = zOffset - height/float(nbPointsForStandUp)
                    t = t + standUpDt
                    self.listOfTimedPositions.append([t, x, y, z + zOffset])
                continue
            else :
                words = line.split(" ")
#                 for w in words :
#                     w = w.strip()
                
                pX = x
                pY = y
                if (isLetter) :
                    x = (scale*float(words[0])/40000.0 + offsetX)*(-1) + 2*(xMoy + offsetX) #Axial symetry
                    y = scale*-1*float(words[1])/40000.0 + offsetY
                else :
                    x = scale*float(words[0])
                    y = scale*float(words[1])
                t = t + dt
#                 if (lineId%reduction == 0) :
#                     lineId = (lineId + 1)%reduction
#                 else :
#                     lineId = (lineId + 1)%reduction
#                     continue
                
            if (previousLineWasU) :
                #We lifted the pen, now we need to smoothly go to the next position and lower the pen again
                previousLineWasU = False
                distance = numpy.sqrt((x-pX)*(x-pX) + (y-pY)*(y-pY))
                travelX = x - pX
                travelY=  y - pY
                dtTravel = 0.02#distance / travelSpeed
#                 print "dtTravel = ", dtTravel
                for i in range(nbPointsTravel) :
                    pX = pX + travelX/float(nbPointsTravel)
                    pY = pY + travelY/float(nbPointsTravel)
                    t = t + dtTravel
                    self.listOfTimedPositions.append([t, x, y, z + zOffset])
                #We need to put the pen on the ground again
                for i in range(nbPointsForStandUp) :
                    zOffset = zOffset + height/float(nbPointsForStandUp)
                    t = t + standUpDt
                    self.listOfTimedPositions.append([t, x, y, z + zOffset])
            else :
                #Normal case
                self.listOfTimedPositions.append([t, x, y, z + zOffset])
                
        print "File read. Returning"
#         print self.listOfTimedPositions
    ### vipCoeff is used to weight more the first and last vipLenght points. This will take effect in the polyFit functions, which minimizes the square error.
    def approximateTraj(self, nbPointsPerPoly=25, polyDegree=4, vipCoeff=4, vipLength=3):
        t = []
        x = []
        y = []
        z = []
        alpha = []
        w = []
        tTemp = []
        xTemp = []
        yTemp = []
        zTemp = []
        alphaTemp = []
        wTemp = []
        t0 = 0
        minDist = 0.13
        maxDist = 0.27
        angleAtMin = numpy.pi
        angleAtMax = 55*numpy.pi/180.0
        print "t0 = ", t0
        previousT = 0
        i = 0
        self.listOfTrajCoefs = []
        self.durations = []
            
        for p in self.listOfTimedPositions :
            tTemp.append(p[0] - t0)
            xTemp.append(p[1])
            yTemp.append(p[2])
            zTemp.append(p[3])
            #Calculating alpha (angle of the pencil, 90 deg means the pencil is vertical)
            d = numpy.sqrt(p[1]*p[1] + p[2]*p[2])
            if (d <= minDist) :
                alphaTemp.append(angleAtMin)
            elif (d >= maxDist) :
                alphaTemp.append(angleAtMax)
            else :
                alphaTemp.append(angleAtMin - (angleAtMin - angleAtMax) * ((d - minDist)/float(maxDist - minDist)))
                
            if ( (i < vipLength) or (i >= (nbPointsPerPoly - vipLength)) ):
                wTemp.append(vipCoeff)
            else :
                wTemp.append(1)
            
            i = i + 1
            previousT = p[0]
            if (i >= nbPointsPerPoly) :
                #End of a portion of time
                i = 0
                duration = int((p[0] - t0)*10000)
                if (duration == 0) :
                    break
                self.durations.append(duration)
                t.append(tTemp)
                x.append(xTemp)
                y.append(yTemp)
                z.append(zTemp)
                alpha.append(alphaTemp)
                w.append(wTemp)
                tTemp = []
                xTemp = []
                yTemp = []
                zTemp = []
                alphaTemp = []
                wTemp = []
                t0 = p[0]
                
                
        if (i != 0) :
            #The last portion of time is missing, adding it
            t.append(tTemp)
            x.append(xTemp)
            y.append(yTemp)
            z.append(zTemp)
            alpha.append(alphaTemp)
            w.append(wTemp)
            #Ugly stuff
            self.durations.append(int((self.listOfTimedPositions[-1][0] - previousT)*10000))
        
        coefsX = []
        coefsY = []
        coefsZ = []
        coefsAlpha = []
#         sum = 0
        print "Before drama"

        for index in range(len(t)) :
            tempCoefsX = numpy.polyfit(t[index], x[index], 3,full=False, w=w[index])
            tempCoefsY = numpy.polyfit(t[index], y[index], 3,full=False, w=w[index])
            tempCoefsZ = numpy.polyfit(t[index], z[index], 3,full=False, w=w[index])
            tempCoefsAlpha = numpy.polyfit(t[index], alpha[index], 3,full=False, w=w[index])
#             try:
#                 tempCoefsX = numpy.polyfit(t[index], x[index], 4,full=False, w=w[index])
#                 tempCoefsY = numpy.polyfit(t[index], y[index], 4,full=False, w=w[index])
#                 tempCoefsZ = numpy.polyfit(t[index], z[index], 4,full=False, w=w[index])
#                 tempCoefsAlpha = numpy.polyfit(t[index], alpha[index], 4,full=False, w=w[index])
#             except numpy.RankWarning:
#                 print "Poorly conditionned with deg 4. Tryng deg 3 !"
#                 tempCoefsX = numpy.polyfit(t[index], x[index], 3,full=False, w=w[index]).insert(0, 0.0)
#                 tempCoefsY = numpy.polyfit(t[index], y[index], 3,full=False, w=w[index]).insert(0, 0.0)
#                 tempCoefsZ = numpy.polyfit(t[index], z[index], 3,full=False, w=w[index]).insert(0, 0.0)
#                 tempCoefsAlpha = numpy.polyfit(t[index], alpha[index], 3,full=False, w=w[index]).insert(0, 0.0)
        

            coefsX.append(tempCoefsX)
            coefsY.append(tempCoefsY)
            coefsZ.append(tempCoefsZ)
            coefsAlpha.append(tempCoefsAlpha)
        print "after drama"
        for index in range(len(coefsX)) :
            #Reversing the order in order to have : a0 + a1*t + etc
            try :
                self.listOfTrajCoefsX.append([coefsX[index][4], coefsX[index][3], coefsX[index][2], coefsX[index][1], coefsX[index][0]])
                self.listOfTrajCoefsY.append([coefsY[index][4], coefsY[index][3], coefsY[index][2], coefsY[index][1], coefsY[index][0]])
                self.listOfTrajCoefsZ.append([coefsZ[index][4], coefsZ[index][3], coefsZ[index][2], coefsZ[index][1], coefsZ[index][0]])
                self.listOfTrajCoefsAlpha.append([coefsAlpha[index][4], coefsAlpha[index][3], coefsAlpha[index][2], coefsAlpha[index][1], coefsAlpha[index][0]])
            except :
                self.listOfTrajCoefsX.append([coefsX[index][3], coefsX[index][2], coefsX[index][1], coefsX[index][0], 0.0])
                self.listOfTrajCoefsY.append([coefsY[index][3], coefsY[index][2], coefsY[index][1], coefsY[index][0], 0.0])
                self.listOfTrajCoefsZ.append([coefsZ[index][3], coefsZ[index][2], coefsZ[index][1], coefsZ[index][0], 0.0])
                self.listOfTrajCoefsAlpha.append([coefsAlpha[index][3], coefsAlpha[index][2], coefsAlpha[index][1], coefsAlpha[index][0], 0.0])
#         plt.plot(t[0], y[0], 
#                  t[1] + t[0][len(t[0]) - 1], y[1], 
#                  t[2] + t[1][len(t[1]) - 1], y[2], 
#                  t[3] + t[2][len(t[2]) - 1], y[3], 
#                  t[4] + t[3][len(t[3]) - 1], y[4], 
#                  t[5] + t[4][len(t[4]) - 1], y[5])
#         plt.show()

    def getPlottable(self):
        t = []
        x = []
        y = []
        z = []
        for p in self.listOfTimedPositions :
            t.append(p[0])
            x.append(p[1])
            y.append(p[2])
            z.append(p[3])
        return t, x, y, z
    
def evalPoly(x, coefs):
    result = coefs[0] + coefs[1]*x + coefs[2]*x**2 + coefs[3]*x**3 + coefs[4]*x**4 
    return result
    
def smooth(y, size):
    box = numpy.ones(size)/size
    ySmooth = numpy.convolve(y, box, mode='same')
    return ySmooth   
    
    