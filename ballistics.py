import mpmath
import numpy
from sympy import solve, Poly, Eq, Function, exp, cos, sin, tan, sqrt, Symbol, log, I
from sympy.core.evalf import N
from sympy.simplify import simplify

import matplotlib.pyplot as plt


# from sympy.abc import x, y, z, a, b
f = Function('f')

def main() :
#     x = Symbol('x')
    x = 1 #m
    v0 = Symbol('v0')
    theta = Symbol('theta')
#     theta = numpy.pi/4 #rad
#     v0 = 0.1 #m/s
    m = 2.3*0.001 #kg
    z0 = 1 #m
    g = 9.80665 #m/s**2
    
    delta = tan(theta)*tan(theta) + 4*g*z0/(2*m*v0*v0*cos(theta)*cos(theta))
#     x = (-m*v0*v0*cos(theta)*cos(theta)/g)*(-tan(theta) + sqrt(delta))
    
#     result = solve((-m*v0*v0*cos(theta)*cos(theta)/g)*(-tan(theta) - sqrt(delta)) - x)
#     print result

    # for + sqrt :
    solutions1 = [
     {theta: I*(-log(-sqrt((2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 - 1.25e+46)/v0**2)*sqrt(-1.0e-29 - 9.99999999999999e-30*I)) + 15.8511652148731)}, 
     {theta: I*(-log(sqrt((2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 - 1.25e+46)/v0**2)*sqrt(-1.0e-29 - 9.99999999999999e-30*I)) + 15.8511652148731)}, 
     {theta: I*(-log(-sqrt((1.0e-29 + 9.99999999999999e-30*I)*(-2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 + 1.25e+46)/v0**2)) + 15.8511652148731)}, 
     {theta: I*(-log(sqrt((1.0e-29 + 9.99999999999999e-30*I)*(-2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 + 1.25e+46)/v0**2)) + 15.8511652148731)}
     ]
    # for - sqrt :
    solutions2 = [
     {theta: I*(-log(-sqrt((2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 - 1.25e+46)/v0**2)*sqrt(-1.0e-29 - 9.99999999999999e-30*I)) + 15.8511652148731)}, 
     {theta: I*(-log(sqrt((2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 - 1.25e+46)/v0**2)*sqrt(-1.0e-29 - 9.99999999999999e-30*I)) + 15.8511652148731)}, 
     {theta: I*(-log(-sqrt((1.0e-29 + 9.99999999999999e-30*I)*(-2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 + 1.25e+46)/v0**2)) + 15.8511652148731)}, 
     {theta: I*(-log(sqrt((1.0e-29 + 9.99999999999999e-30*I)*(-2.93168411231155e+42*v0**2 + 1.58113883008419e+23*(-3.43790869375198e+38*v0**4 - 2.93168411231155e+42*v0**2 + 6.25e+45)**0.5 + 1.25e+46)/v0**2)) + 15.8511652148731)}]
    
#     2.54006868749427 - 4.73851732757771e-14*I
#     -0.601523966095522 - 4.73851732757771e-14*I
#     1.38692212949297 - 4.73404969067859e-14*I
#     1.75467052409682 - 4.73404969067859e-14*I
    for d in solutions1 :
        for key in d :
            theta = (N(d[key].subs(v0, 100)))
            theta = 1.75467052409682
            print theta
            v0 = 100
            pos = lambda t : -g/(2*m)*t*t + v0*sin(theta)*t + z0
            t = numpy.arange(0, 3, 0.1)
            plt.plot(t, pos(t))
            plt.show()
            print "evaluation = ", eval
            print "position = ", N(pos(eval))
    for d in solutions2 :
        for key in d :
            print "evaluation = ", N(d[key].subs(v0, 1))
#     print thetaTest


if __name__ == '__main__' :
    main()