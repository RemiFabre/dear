import timedTrajectory
import pickle
import numpy

class trajectoryRecorder(object):
    def __init__(self, motors) :
        self.motors = motors
        self.listOfTimedTrajectories = []
        for m in motors :
            self.listOfTimedTrajectories.append(timedTrajectory.TimedTrajectory(m))
        
    def tick(self, time):
        for t in self.listOfTimedTrajectories :
            if t.motor.updated == False :
                #We'll add a point to the trajectories only if all the motors have been updated
                return
        
        for t in self.listOfTimedTrajectories :
            t.update(time)
        
    def appendTrajectoryRecorder(self, newTrajRecorder):
        i = 0
        for m in self.motors :
            self.listOfTimedTrajectories[i].appendTraj(newTrajRecorder.listOfTimedTrajectories[i])
            i = i + 1
    def clear(self):
        self.listOfTimedTrajectories = []
        for m in self.motors :
            self.listOfTimedTrajectories.append(timedTrajectory.TimedTrajectory(m))
    def save(self, title):
        pickle.dump(self, open(title, "wb" ) )
        
    @staticmethod
    def load(title, motors):
        trajRecorder = pickle.load( open(title, "rb" ) )
        #The motors shouldn't be read out of a serialization.
        trajRecorder.motors = motors
        for t in trajRecorder.listOfTimedTrajectories :
            motorId = t.motor.id
            #Looking for the motor in the actual list of motors
            for m in motors :
                if (m.id == motorId) :
                    t.motor = m
        return trajRecorder
    
    def delta(self, trajRecorder, motors):
        result  = trajectoryRecorder(motors)
        tempListOfTimedTrajectories = []
        i = 0
        for selfTimedTraj, timedTraj in zip(self.listOfTimedTrajectories, trajRecorder.listOfTimedTrajectories) :
            #Getting the delta between 2 timed trajectories
            deltaTimedTraj = selfTimedTraj.delta(timedTraj)
            tempListOfTimedTrajectories.append(deltaTimedTraj)
            i = i + 1
        result.listOfTimedTrajectories = tempListOfTimedTrajectories
        return result
    
    def readFromFile(self, fileAddress, fromLeph=False, fromLephTorque=False):
        file = open(fileAddress)
        id = -1
        durationRead = False
        sign = 1
        for line in file :
            if (len(line) < 2) :
                continue
            if (line.startswith("motor")) :
                words = line.split('_')
                if len(words) != 2 :
                    raise("Expected 'motor_id', found {0}".format(line))
                id = int(words[1])
                if id == 2 or id == 3 or id == 4:
                    sign = -1
                else :
                    sign = 1
                for traj in self.listOfTimedTrajectories :
                    if (traj.motor.id == id) :
                        #Found our motor
                        print "Reading coefs for motor {0}".format(id)
                        currentTraj = traj
                        currentTraj.clear
                        durationRead = False
            else :
                if durationRead == False :
                    durationRead = True
                    duration = float(line.strip())
                    currentTraj.durations.append(int(duration*10000))
                else :
                    durationRead = False
                    words = line.split(',')
                    for i in range(len(words)) :
                        words[i] = words[i].strip()
    
                    if (len(words) != 5) :
                        raise("Expected 'c0, c1, C2, c3, c4', found {0}".format(line))
                    listOfCoefs = []
                    for coef in words :
                        if fromLeph :
                            value = float(coef)*(180.0/numpy.pi)*(4096/360.0)*sign
                            listOfCoefs.append(value)
                        elif fromLephTorque :
                            value = float(coef)*(4096/(2*numpy.pi))*sign
                            listOfCoefs.append(value)
                        else :
                            value = float(coef)
                            listOfCoefs.append(value)
                    currentTraj.listOfTrajCoefs.append(listOfCoefs)
    def addBSTrajs(self):
        for t in self.listOfTimedTrajectories :
            t.addBSTraj()         
    
    def __repr__(self):
        result = ""
        for p in self.listOfTimedTrajectories :
            result = result + repr(p)
        return result

                        
