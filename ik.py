from __future__ import division

import math

import utils
import configuration 


# Given the sizes (a, b, c) of the 3 sides of a triangle, returns the angle between a and b using the alKashi theorem.
def alKashi(a, b, c, sign=-1):
    value = ((a*a)+(b*b)-(c*c))/(2*a*b)
    #Note : to get the other altenative, simply change the sign of the return :
    print "a = ", a, "; b = ", b, "; c = ", c, "; value = ", value
    return sign * math.acos(value)


# Computes the inverse kinematics of the arm
# Given the destination point (x, y, z) and alpha the incidence angle of the last section of the arm
# returns the angles to apply to the 4 axes
def computeIK(x, y, z, alpha, l1=configuration.constL0, l2=configuration.constL1,l3=configuration.constL2, l4=0) :
    if (x < 0) :
        #No BS pls
        x = 0
        
    alpha = -math.radians(alpha)
    #print ("Destination : " + str(x) + " " + str(y) + " " + str(z))
    # theta0 is simply the angle of the leg in the X/Y plane. We have the first angle we need.
    theta0 = math.degrees(math.atan2(y, x))
    theta1 = 0 #Cos yeah.
    theta4 = 0 #Because reasons.

    # Distance between the second motor and the projection of the end of the leg on the X/Y plane
    d = math.sqrt(x*x + y*y) - l1

    if (d < 0) :
        print("Destination point too close")
        d = 0

    if (d > l2+l3+l4):
        print("Destination point too far away")
        d = l2+l3+l4
    z3 = z + l4*math.sin(alpha)

    d13 = math.sqrt(math.pow(d-l4*math.cos(alpha),2) + z3*z3 )

    # The Al Kashi law nails it
    theta2 = math.degrees(alKashi(d13, d - l4*math.cos(alpha), z3, utils.sign(z3)) - alKashi(l2, d13, l3, utils.sign(z3)))
    theta3 = math.degrees(-math.pi + alKashi(l2, l3, d13, utils.sign(z3)))
    theta4 = ((math.degrees(alpha) - theta2 + theta1 + configuration.theta3Correction) * configuration.theta3Direction)%360


    #Applying the corrections
    theta0 = ((theta0 + configuration.theta0Correction) * configuration.theta0Direction)%360
    #Classical -1
    theta1 = ((theta1 + configuration.theta1Correction) * configuration.theta1Direction*-1)%360
    theta2 = ((theta2 + configuration.theta2Correction) * configuration.theta2Direction)%360

    return [theta0, theta1, theta2, theta3]

#Returns an angle that's between -180 and 180
def modulo180(angle) :
    if (-180 < angle < 180) :
        return angle

    angle  = angle % 360
    if (angle > 180) :
        return -360 + angle

    return angle


if __name__ == '__main__':
    x = 0#constL0 + constL1 + constL2 + constL3
    y = 0
    z = 220
    alpha = 0
    print "IK of [", x, ", ", y, ", ", z, "] = ", computeIK(x, y, z, alpha)
    x = -5#constL0 + constL1 + constL2 + constL3
    y = 0
    z = 220
    alpha = 0
    print "IK of [", x, ", ", y, ", ", z, "] = ", computeIK(x, y, z, alpha)
    

