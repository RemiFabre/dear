"""
Simple demo of a scatter plot.
"""
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
	N = 50
	x = np.random.rand(N)
	y = np.random.rand(N)
	colors = np.random.rand(N)
	area = np.pi * (15 * np.random.rand(N))**2 # 0 to 15 point radiuses
	
	plt.scatter(x, y, s=area, c=colors, alpha=0.5)
	plt.show()
	
	
	# evenly sampled time at 100ms intervals
	t = np.arange(-2.5, 2.5, 0.1)
	
	# red dashes, blue squares and green triangles
	plt.plot(t, t, 'r--', t, t**2, 'bs', t, np.cos(4*t)*t**2, 'g^')
	plt.show()