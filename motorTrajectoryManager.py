import motorTrajectory
import utils

class MotorTrajectoryManager():
    def __init__(self, listOfMotorTrajs=[]) :
        self.listOfMotorTrajs = listOfMotorTrajs
        
    def addMotorTrajectory(self, motorTraj) :
        self.listOfMotorTrajs.append(motorTraj)
    
    def createMotorTrajectory(self, motor, listOfTrajs, listOfTorqueTrajs, listOfDurations, mode=3, repeat=False, delay=0.1) :
        motorTraj = motorTrajectory.MotorTrajectory(motor, listOfTrajs, listOfTorqueTrajs, listOfDurations, mode, repeat, delay)
        self.listOfMotorTrajs.append(motorTraj)
        
    def clear(self):
        self.listOfMotorTrajs = []
        
    def start(self) :
        for t in self.listOfMotorTrajs :
            t.start()
        
    def tick(self) :
        stillWorking = False
        for t in self.listOfMotorTrajs :
#             utils.updateGoalPosition(t.motor)
            if (t.over != True) :
                t.tick()
                stillWorking = True
            elif (t.stopped != True) :
                utils.stopTrajectory(t.motor)
                t.stopped= True
        return stillWorking
    
    def stopAll(self) :
        for t in self.listOfMotorTrajs :
            if (t.stopped != True) :
#                 utils.updateGoalPosition(t.motor)
                utils.stopTrajectory(t.motor)    
                pass                    

    def __repr__(self):
        return "listOfMotorTrajectories : " + self.listOfMotorTrajs + "\n"